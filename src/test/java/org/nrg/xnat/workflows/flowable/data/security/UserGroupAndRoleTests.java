package org.nrg.xnat.workflows.flowable.data.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class UserGroupAndRoleTests {
    @Test
    public void testRoleCreation() {

    }

    @Test
    public void testUserGroupAndRoleCreation() {
        final UserPrincipal system = _service.findUser("system");

        final Map<String, Role>      roles      = _service.createRolesAndTemplates(system, Arrays.asList("qb", "wr", "rb", "dl", "db"));
        final Map<String, UserGroup> userGroups = _service.createUserGroups(system, "early", "middle", "modern");

        final Map<String, UserPrincipal> users = PLAYERS.entrySet().stream()
                                                        .collect(Collectors.toMap(Map.Entry::getKey,
                                                                                  entry -> _service.saveUser(system, new UserPrincipal(entry.getKey(),
                                                                                                                                       entry.getValue().get(Attr.password),
                                                                                                                                       entry.getValue().get(Attr.fullName),
                                                                                                                                       entry.getValue().get(Attr.email),
                                                                                                                                       Collections.singleton(roles.get(entry.getValue().get(Attr.role))),
                                                                                                                                       Collections.singleton(userGroups.get(entry.getValue().get(Attr.group)))))));
        users.keySet().forEach(name -> {
            final UserPrincipal     user     = _service.findUser(name);
            final Map<Attr, String> expected = PLAYERS.get(name);

            assertThat(user).isNotNull();
            assertThat(user.getPassword()).isEqualTo(expected.get(Attr.password));
            assertThat(user.getFullName()).isEqualTo(expected.get(Attr.fullName));
            assertThat(user.getEmail()).isEqualTo(expected.get(Attr.email));
            assertThat(user.getRoles()).containsExactly(_service.findRole(expected.get(Attr.role)));
            assertThat(user.getGroups()).containsExactly(_service.findUserGroup(expected.get(Attr.group)));
        });
    }

    private enum Attr {password, fullName, email, role, group}
    private static final Map<String, Map<Attr, String>> PLAYERS = new HashMap<String, Map<Attr, String>>() {{
        put("lselmon", new HashMap<Attr, String>() {{
            put(Attr.password, "63ftw");
            put(Attr.fullName, "Lee Roy Selmon");
            put(Attr.email, "leeroy@buccaneers.com");
            put(Attr.role, "dl");
            put(Attr.group, "early");
        }});
        put("dwilliams", new HashMap<Attr, String>() {{
            put(Attr.password, "12ftw");
            put(Attr.fullName, "Doug Williams");
            put(Attr.email, "doug@buccaneers.com");
            put(Attr.role, "qb");
            put(Attr.group, "early");
        }});
        put("rbell", new HashMap<Attr, String>() {{
            put(Attr.password, "42ftw");
            put(Attr.fullName, "Ricky Bell");
            put(Attr.email, "ricky@buccaneers.com");
            put(Attr.role, "rb");
            put(Attr.group, "early");
        }});
        put("khouse", new HashMap<Attr, String>() {{
            put(Attr.password, "89ftw");
            put(Attr.fullName, "Kevin House");
            put(Attr.email, "kevin@buccaneers.com");
            put(Attr.role, "wr");
            put(Attr.group, "early");
        }});
        put("wsapp", new HashMap<Attr, String>() {{
            put(Attr.password, "99ftw");
            put(Attr.fullName, "Warren Sapp");
            put(Attr.email, "warren@buccaneers.com");
            put(Attr.role, "dl");
            put(Attr.group, "middle");
        }});
        put("dbrooks", new HashMap<Attr, String>() {{
            put(Attr.password, "55ftw");
            put(Attr.fullName, "Derrick Brooks");
            put(Attr.email, "derrick@buccaneers.com");
            put(Attr.role, "db");
            put(Attr.group, "middle");
        }});
        put("rbarber", new HashMap<Attr, String>() {{
            put(Attr.password, "20ftw");
            put(Attr.fullName, "Ronde Barber");
            put(Attr.email, "ronde@buccaneers.com");
            put(Attr.role, "db");
            put(Attr.group, "middle");
        }});
        put("jlynch", new HashMap<Attr, String>() {{
            put(Attr.password, "47ftw");
            put(Attr.fullName, "John Lynch");
            put(Attr.email, "john@buccaneers.com");
            put(Attr.role, "db");
            put(Attr.group, "middle");
        }});
        put("jwinston", new HashMap<Attr, String>() {{
            put(Attr.password, "3ftw");
            put(Attr.fullName, "Jameis Winston");
            put(Attr.email, "jameis@buccaneers.com");
            put(Attr.role, "qb");
            put(Attr.group, "modern");
        }});
        put("gmccoy", new HashMap<Attr, String>() {{
            put(Attr.password, "93ftw");
            put(Attr.fullName, "Gerald McCoy");
            put(Attr.email, "gerald@buccaneers.com");
            put(Attr.role, "dl");
            put(Attr.group, "modern");
        }});
        put("mevans", new HashMap<Attr, String>() {{
            put(Attr.password, "13ftw");
            put(Attr.fullName, "Mike Evans");
            put(Attr.email, "mike@buccaneers.com");
            put(Attr.role, "wr");
            put(Attr.group, "modern");
        }});
    }};
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private IdentityManagementService _service;
}
