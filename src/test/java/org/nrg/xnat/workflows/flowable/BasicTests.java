package org.nrg.xnat.workflows.flowable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.workflows.flowable.data.AggregatedPermission;
import org.springframework.security.acls.model.Permission;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

@RunWith(SpringRunner.class)
public class BasicTests {
    @Test
    public void testLoadSqlFiles() {
        final URL h2schema = ClassLoader.getSystemClassLoader().getResource("createAclSchema.sql");
        final URL pgschema = ClassLoader.getSystemClassLoader().getResource("createAclSchemaPostgres.sql");
        assertThat(h2schema).isNotNull();
        assertThat(pgschema).isNotNull();
    }

    @Test
    public void testListToMapTransform() {
        // This is a simple test to verify a function used in JpaIdentityManagementServiceImpl to transform settings
        // from the application properties into a map for initializing roles.
        final List<String> groupRoles  = Arrays.asList("owner", "member", "collaborator");
        final List<String> permissions = Arrays.asList("4", "3", "1");

        final Map<String, Integer> mappedValues = new HashMap<>();
        IntStream.range(0, groupRoles.size()).boxed().collect(Collectors.toMap(groupRoles::get, permissions::get)).forEach((groupRole, value) -> {
            final int topPermission = Integer.parseInt(value);
            mappedValues.put(groupRole, topPermission);
        });
        assertThat(mappedValues.size()).isEqualTo(3);
        assertThat(mappedValues).containsExactly(entry("owner", 4), entry("member", 3), entry("collaborator", 1));
    }

    @Test
    public void testPermissions() {
        final AggregatedPermission permission = new AggregatedPermission(AggregatedPermission.READ, AggregatedPermission.WRITE, AggregatedPermission.CREATE, AggregatedPermission.DELETE, AggregatedPermission.ADMINISTRATION);
        assertThat(permission.getMask()).isEqualTo(0b11111);
        assertThat(permission.getPattern()).matches("^[.]{27}[^.]{5}$");
        assertThat(permission.disaggregate()).containsExactly(AggregatedPermission.READ, AggregatedPermission.WRITE, AggregatedPermission.CREATE, AggregatedPermission.DELETE, AggregatedPermission.ADMINISTRATION);

        permission.clear(AggregatedPermission.ADMINISTRATION);
        assertThat(permission.getMask()).isEqualTo(0b1111);
        assertThat(permission.getPattern()).matches("^[.]{28}[^.]{4}$");

        final List<Permission> permissions = permission.disaggregate();
        assertThat(permissions).isNotNull();
        assertThat(permissions).isNotEmpty();
        assertThat(permissions.size()).isEqualTo(4);
        assertThat(permissions.stream().mapToInt(Permission::getMask).boxed().collect(Collectors.toList())).containsExactly(basePermissionValues);
    }

    private static final Integer[] basePermissionValues = Stream.of(AggregatedPermission.READ, AggregatedPermission.WRITE, AggregatedPermission.CREATE, AggregatedPermission.DELETE).mapToInt(Permission::getMask).boxed().collect(Collectors.toList()).toArray(new Integer[4]);
}
