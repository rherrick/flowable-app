package org.nrg.xnat.workflows.flowable.data.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.workflows.flowable.FlowableApplication;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.TagRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ExperimentAssessorRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ExperimentRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ProjectRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.SubjectRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.RoleRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CommandLineRunner.class), excludeAutoConfiguration = FlowableApplication.class)
@Transactional
@ActiveProfiles("test")
public class NodeTests {
    @Test
    public void createBasicProjects() {
        final Project project1 = new Project("project1");
        final Project project2 = new Project("project2");
        entityManager.persist(project1);
        entityManager.persist(project2);
        entityManager.flush();

        final Project retrieved1 = projects.findByLabel("project1");
        assertThat(retrieved1).isNotNull();
        assertThat(retrieved1.getLabel()).isEqualTo(project1.getLabel());
        final Project retrieved2 = projects.findByLabel("project2");
        assertThat(retrieved2).isNotNull();
        assertThat(retrieved2.getLabel()).isEqualTo(project2.getLabel());
    }

    @Test
    public void createProjectWithSubject() {
        final Project project = new Project("project");
        entityManager.persist(project);
        entityManager.flush();

        final Project retrievedProject = projects.findByLabel("project");
        assertThat(retrievedProject).isNotNull();
        assertThat(retrievedProject.getLabel()).isEqualTo(project.getLabel());

        final Subject subject = new Subject("subject", project);
        entityManager.persist(subject);
        entityManager.flush();

        final Subject retrievedSubject = subjects.findByLabel("subject");
        final Project parent           = retrievedSubject.getProject();
        assertThat(retrievedSubject).isNotNull();
        assertThat(parent).isNotNull();
        assertThat(retrievedSubject.getLabel()).isEqualTo(subject.getLabel());
        assertThat(retrievedSubject.getName()).isEqualTo(subject.getName());
        assertThat(parent).isEqualTo(project);
    }

    @Test
    public void createHierarchicalProjects() {
        final Project project1  = new Project("project1");
        final Project project2  = new Project("project2");
        final Project project11 = new Project("project11", project1);
        final Project project21 = new Project("project21", project2);
        entityManager.persist(project1);
        entityManager.persist(project2);
        entityManager.persist(project11);
        entityManager.persist(project21);
        entityManager.flush();

        final Project retrieved1 = projects.findByLabel("project1");
        assertThat(retrieved1.getLabel()).isEqualTo(project1.getLabel());
        final Project retrieved2 = projects.findByLabel("project2");
        assertThat(retrieved2.getLabel()).isEqualTo(project2.getLabel());
        final Project retrieved11 = projects.findByLabel("project11");
        assertThat(retrieved11).isNotNull();
        assertThat(retrieved11.getLabel()).isEqualTo(project11.getLabel());
        final Project retrieved21 = projects.findByLabel("project21");
        assertThat(retrieved21).isNotNull();
        assertThat(retrieved21.getLabel()).isEqualTo(project21.getLabel());
    }

    @Test
    public void createProjectSubjectExperimentHierarchyStreamed() {
        final NumberFormat formatter = NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(3);
        IntStream.range(1, 3).mapToObj(formatter::format).forEach(projectNumber -> {
            final Project project = new Project(projectNumber, "Project " + projectNumber);
            entityManager.persistAndFlush(project);
            IntStream.range(1, 3).mapToObj(formatter::format).forEach(subjectNumber -> {
                final String  label   = projectNumber + "_" + subjectNumber;
                final Subject subject = new Subject(label, "Subject " + label, project);
                entityManager.persistAndFlush(subject);
                IntStream.range(1, 3).mapToObj(formatter::format).forEach(experimentNumber -> {
                    final Experiment experiment = new Experiment(projectNumber + "_" + subjectNumber + "_" + experimentNumber, subject);
                    entityManager.persistAndFlush(experiment);
                });
            });
        });

    }

    @Test
    public void createProjectSubjectExperimentHierarchy() {
        final List<Project>    projects    = new ArrayList<>();
        final List<Subject>    subjects    = new ArrayList<>();
        final List<Experiment> experiments = new ArrayList<>();
        getProjects().forEach(project -> {
            entityManager.persistAndFlush(project);
            projects.add(project);
            getSubjects(project).forEach(subject -> {
                entityManager.persistAndFlush(subject);
                subjects.add(subject);
                getExperiments(subject).forEach(experiment -> {
                    entityManager.persistAndFlush(experiment);
                    experiments.add(experiment);
                });
            });
        });

        assertThat(projects.size()).isEqualTo(3);
        assertThat(subjects.size()).isEqualTo(9);
        assertThat(experiments.size()).isEqualTo(27);
    }

    private List<Project> getProjects() {
        return Arrays.asList(new Project("001", "Project 001"),
                             new Project("002", "Project 002"),
                             new Project("003", "Project 003"));
    }

    private List<Subject> getSubjects(final Project project) {
        return Arrays.asList(new Subject(project.getLabel() + "_001", project),
                             new Subject(project.getLabel() + "_002", project),
                             new Subject(project.getLabel() + "_003", project));
    }

    private List<Experiment> getExperiments(final Subject subject) {
        return Arrays.asList(new Experiment(subject.getLabel() + "_001", subject),
                             new Experiment(subject.getLabel() + "_002", subject),
                             new Experiment(subject.getLabel() + "_003", subject));
    }
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ExperimentAssessorRepository experimentAssessors;
    @Autowired
    private ExperimentRepository         experiments;
    @Autowired
    private ProjectRepository            projects;
    @Autowired
    private RoleRepository               roles;
    @Autowired
    private SubjectRepository            subjects;
    @Autowired
    private TagRepository                tags;
    @Autowired
    private UserRepository               users;
}
