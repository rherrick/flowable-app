package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl implements CustomRoleRepository {
    @Autowired
    @Lazy
    public void setRoleRepository(final RoleRepository roles) {
        _roles = roles;
    }

    @Override
    public <T extends EntityNode> Role getRoleForEntity(final String name, final T entity) {
        return _roles.findByNameAndEntityClassAndEntityId(name, entity.getClass().getName(), entity.getId());
    }

    @Override
    public <T extends EntityNode> Role getRoleForEntity(final String name, final Class<T> entityClass, final long entityId) {
        return _roles.findByNameAndEntityClassAndEntityId(name, entityClass.getName(), entityId);
    }
    private RoleRepository _roles;
}
