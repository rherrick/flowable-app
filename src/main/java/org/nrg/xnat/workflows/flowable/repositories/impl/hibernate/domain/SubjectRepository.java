package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain;

import org.nrg.xnat.workflows.flowable.data.domain.Project;
import org.nrg.xnat.workflows.flowable.data.domain.Subject;
import org.nrg.xnat.workflows.flowable.data.domain.Tag;
import org.nrg.xnat.workflows.flowable.repositories.EntityNodeRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface SubjectRepository extends EntityNodeRepository<Subject> {
    List<Subject> findAllByProject(final Project parent);

    List<Subject> findByTags(final Tag... tags);

    Subject findByLabel(final String label);

    Subject findByDateOfBirthAfter(final Date date);
}
