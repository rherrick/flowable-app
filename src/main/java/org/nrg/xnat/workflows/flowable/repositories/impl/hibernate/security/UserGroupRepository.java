package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.security.UserGroup;
import org.nrg.xnat.workflows.flowable.repositories.WorkbenchRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface UserGroupRepository extends WorkbenchRepository<UserGroup> {
    boolean existsByName(final String name);

    UserGroup findByName(final String name);

    List<UserGroup> findAllByNameIn(final List<String> names);

    List<UserGroup> findAllByUsers(final List<String> usernames);

    List<UserGroup> findAllByRoles(final List<String> roles);
}
