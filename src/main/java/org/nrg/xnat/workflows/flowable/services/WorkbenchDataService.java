package org.nrg.xnat.workflows.flowable.services;

import org.nrg.xnat.workflows.flowable.data.domain.*;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;

import java.util.List;

@SuppressWarnings("unused")
public interface WorkbenchDataService {
    List<Project> getProjects();

    Long getProjectId(final String label);

    Project getProject(final long id);

    Project getProject(final String label);

    Project saveProject(final UserPrincipal user, final Project project);

    void deleteProject(final UserPrincipal user, final long id);

    void deleteProject(final UserPrincipal user, final String label);

    void deleteProject(final UserPrincipal user, final Project project);

    List<Subject> getSubjects();

    Subject getSubject(final long id);

    Subject getSubject(final String label);

    Subject saveSubject(final UserPrincipal user, final Subject subject);

    void deleteSubject(final UserPrincipal user, final long id);

    void deleteSubject(final UserPrincipal user, final String label);

    void deleteSubject(final UserPrincipal user, final Subject subject);

    List<Experiment> getExperiments();

    Experiment getExperiment(final long id);

    Experiment getExperiment(final String label);

    Experiment saveExperiment(final UserPrincipal user, final Experiment experiment);

    void deleteExperiment(final UserPrincipal user, final long id);

    void deleteExperiment(final UserPrincipal user, final String label);

    void deleteExperiment(final UserPrincipal user, final Experiment experiment);

    List<ExperimentAssessor> getExperimentAssessors();

    ExperimentAssessor getExperimentAssessor(final long id);

    ExperimentAssessor getExperimentAssessor(final String label);

    ExperimentAssessor saveExperimentAssessor(final UserPrincipal user, final ExperimentAssessor experimentAssessor);

    void deleteExperimentAssessor(final UserPrincipal user, final long id);

    void deleteExperimentAssessor(final UserPrincipal user, final String label);

    void deleteExperimentAssessor(final UserPrincipal user, final ExperimentAssessor experimentAssessor);

    List<Tag> getTags();

    Tag getTag(final long id);

    Tag getTag(final String value);

    Tag saveTag(final UserPrincipal user, final String value, final String newValue);

    Tag saveTag(UserPrincipal user, String value);

    Tag saveTag(final UserPrincipal user, final Tag tag);

    void deleteTag(final UserPrincipal user, final long id);

    void deleteTag(final UserPrincipal user, final String value);

    void deleteTag(final UserPrincipal user, final Tag tag);

    <T extends EntityNode> void addOwner(final UserPrincipal actor, final UserPrincipal user, final T entity);

    <T extends EntityNode> void addOwners(final UserPrincipal actor, final List<UserPrincipal> users, final T entity);

    <T extends EntityNode> void addMember(final UserPrincipal actor, final UserPrincipal user, final T entity);

    <T extends EntityNode> void addMembers(final UserPrincipal actor, final List<UserPrincipal> users, final T entity);

    <T extends EntityNode> void addCollaborator(final UserPrincipal actor, final UserPrincipal user, final T entity);

    <T extends EntityNode> void addCollaborators(final UserPrincipal actor, final List<UserPrincipal> users, final T entity);

    List<? extends EntityNode> getNodeChildren(final String type, final long parentId);
}
