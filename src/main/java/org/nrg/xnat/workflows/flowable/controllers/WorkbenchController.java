package org.nrg.xnat.workflows.flowable.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Controller
public class WorkbenchController {
    @Autowired
    public WorkbenchController(final RequestMappingHandlerMapping mapping) {
        _mapping = mapping;
    }

    @RequestMapping(value = "endPoints", method = RequestMethod.GET)
    public String getEndPointsInView(Model model) {
        return "admin/endPoints";
    }

    @RequestMapping("/workbench")
    public String workbench(final Model model) {
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "workbench";
    }

    @RequestMapping("/endpoints")
    public String endpoints(final Model model) {
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("endPoints", _mapping.getHandlerMethods().keySet());
        return "endpoints";
    }
    private final RequestMappingHandlerMapping _mapping;
}
