package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.repositories.WorkbenchRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface UserRepository extends WorkbenchRepository<UserPrincipal> {
    boolean existsByUsername(final String username);

    UserPrincipal findByUsername(final String username);

    UserPrincipal findByUsernameAndPassword(final String username, final String password);

    List<UserPrincipal> findAllByUsernameIn(final List<String> usernames);
}
