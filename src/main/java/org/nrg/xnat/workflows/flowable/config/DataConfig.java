package org.nrg.xnat.workflows.flowable.config;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.AbstractWorkbenchRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Optional;

@Configuration
@EnableJpaRepositories(basePackages = "org.nrg.xnat.workflows.flowable.repositories", repositoryBaseClass = AbstractWorkbenchRepository.class)
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableTransactionManagement
public class DataConfig {
    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        final DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setDatabasePopulator(new ResourceDatabasePopulator(StringUtils.equals("h2", platform) ? h2Schema : postgresqlSchema));
        return initializer;
    }

    @Bean
    public AuditorAware<String> auditorAware() {
        return () -> {
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            final String authType;
            if (authentication == null) {
                authType = "system";
            } else {
                final Object principal = authentication.getPrincipal();
                if (principal == null) {
                    authType = "system";
                } else if (principal instanceof String) {
                    authType = (String) principal;
                } else if (principal instanceof UserDetails) {
                    authType = ((UserDetails) principal).getUsername();
                } else {
                    authType = principal.toString();
                }
            }
            return Optional.of(authType);
        };
    }
    @Value("${spring.datasource.platform:h2}")
    private String   platform;
    @Value("classpath:schema-h2.sql")
    private Resource h2Schema;
    @Value("classpath:schema-postgresql.sql")
    private Resource postgresqlSchema;
}
