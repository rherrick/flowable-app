package org.nrg.xnat.workflows.flowable.services;

import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;

import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public interface PermissionsService {
    <T extends EntityNode> void addOwner(final UserPrincipal user, final T entity);

    <T extends EntityNode> void addMember(UserPrincipal user, T entity);

    <T extends EntityNode> void addCollaborator(UserPrincipal user, T entity);

    List<Map<String, String>> getAllSids();

    List<Map<String, String>> getAllClasses();

    List<Map<String, String>> getAllObjectIdentities();

    List<Map<String, String>> getAllEntries();

    ObjectIdentity getIdentity(final EntityNode entity);

    MutableAcl getEntityAcl(final EntityNode entity);

    MutableAcl getAclById(final ObjectIdentity identity);

    MutableAcl createAcl(final ObjectIdentity identity);

    MutableAcl updateAcl(final MutableAcl acl);

    void addSecurityIdToAcl(final MutableAcl acl, final Sid sid, final List<Permission> permissions);

    void deleteAcl(final ObjectIdentity identity);

    <T extends EntityNode> void deleteAcl(final Class<T> entityClass, final long id);
}
