package org.nrg.xnat.workflows.flowable.data.security;

import lombok.*;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The user group provides a way to associate multiple {@link Role roles} with multiple {@link UserPrincipal users}.
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserGroup extends BaseMetadata implements Serializable {
    public UserGroup(final String name) {
        setName(name);
    }
    @NonNull
    @Column(nullable = false, unique = true)
    private String             name;
    private String             description;
    @ManyToMany(fetch = FetchType.EAGER)
    @Builder.Default
    private Set<UserPrincipal> users = new HashSet<>();
    @ManyToMany(fetch = FetchType.EAGER)
    @Builder.Default
    private Set<Role>          roles = new HashSet<>();
}
