package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain;

import org.nrg.xnat.workflows.flowable.data.domain.Experiment;
import org.nrg.xnat.workflows.flowable.data.domain.ExperimentAssessor;
import org.nrg.xnat.workflows.flowable.data.domain.Tag;
import org.nrg.xnat.workflows.flowable.repositories.EntityNodeRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ExperimentAssessorRepository extends EntityNodeRepository<ExperimentAssessor> {
    List<ExperimentAssessor> findAllByExperiment(final Experiment experiment);

    List<ExperimentAssessor> findByType(final String type);

    List<ExperimentAssessor> findByTags(final Tag... tags);

    ExperimentAssessor findByLabel(final String label);
}
