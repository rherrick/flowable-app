package org.nrg.xnat.workflows.flowable.services.impl.hibernate;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.workflows.flowable.data.AggregatedPermission;
import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.services.AbstractWorkbenchService;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.nrg.xnat.workflows.flowable.services.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SqlResolve")
@Service
@Slf4j
public class JpaPermissionsServiceImpl extends AbstractWorkbenchService implements PermissionsService {
    @Autowired
    public JpaPermissionsServiceImpl(final IdentityManagementService idmService, final MutableAclService aclService, final JdbcTemplate template) {
        _idmService = idmService;
        _aclService = aclService;
        _template = template;
    }

    @Override
    public <T extends EntityNode> void addOwner(final UserPrincipal user, final T entity) {
        addPermissions(user, entity, _idmService.getOwnerRole(entity));
    }

    @Override
    public <T extends EntityNode> void addMember(final UserPrincipal user, final T entity) {
        // Now grant admin permissions via an access control entry (ACE)
        addPermissions(user, entity, _idmService.getMemberRole(entity));
    }

    @Override
    public <T extends EntityNode> void addCollaborator(final UserPrincipal user, final T entity) {
        addPermissions(user, entity, _idmService.getCollaboratorRole(entity));
    }

    @Override
    public List<Map<String, String>> getAllSids() {
        return _template.query(QUERY_ACL_SID, (results, rowNum) -> new HashMap<String, String>() {{
            put("id", Long.toString(results.getLong("id")));
            put("principal", Boolean.toString(results.getBoolean("principal")));
            put("sid", results.getString("principal"));
        }});
    }

    @Override
    public List<Map<String, String>> getAllClasses() {
        return _template.query(QUERY_ACL_CLASS, (results, rowNum) -> new HashMap<String, String>() {{
            put("id", Long.toString(results.getLong("id")));
            put("class", results.getString("class"));
        }});
    }

    @Override
    public List<Map<String, String>> getAllObjectIdentities() {
        return _template.query(QUERY_ACL_OBJ_IDENTITIES, (results, rowNum) -> new HashMap<String, String>() {{
            put("id", Long.toString(results.getLong("id")));
            put("entityClass", results.getString("entityClass"));
            put("entityId", Long.toString(results.getLong("entityId")));
        }});
    }

    @Override
    public List<Map<String, String>> getAllEntries() {
        return _template.query(QUERY_ACL_ENTRIES, (results, rowNum) -> new HashMap<String, String>() {{
            put("id", Long.toString(results.getLong("id")));
            put("entityClass", results.getString("entityClass"));
            put("entityId", Long.toString(results.getLong("entityId")));
            put("aceOrder", Integer.toString(results.getInt("aceOrder")));
            put("sid", results.getString("sid"));
            put("principal", Boolean.toString(results.getBoolean("principal")));
            put("mask", Integer.toString(results.getInt("mask")));
        }});
    }

    @Override
    public ObjectIdentity getIdentity(final EntityNode entity) {
        return new ObjectIdentityImpl(entity);
    }

    @Override
    public MutableAcl getEntityAcl(final EntityNode entity) {
        final ObjectIdentity identity = getIdentity(entity);
        return getAclById(identity);
    }

    @Override
    public MutableAcl getAclById(final ObjectIdentity identity) {
        try {
            return (MutableAcl) _aclService.readAclById(identity);
        } catch (NotFoundException nfe) {
            return _aclService.createAcl(identity);
        }
    }

    @Override
    public MutableAcl createAcl(final ObjectIdentity identity) {
        return _aclService.createAcl(identity);
    }

    @Override
    public MutableAcl updateAcl(final MutableAcl acl) {
        return _aclService.updateAcl(acl);
    }

    @Override
    public void addSecurityIdToAcl(final MutableAcl acl, final Sid sid, final List<Permission> permissions) {
        permissions.forEach((Permission permission) -> {
            if (permission instanceof AggregatedPermission && ((AggregatedPermission) permission).isAggregated()) {
                ((AggregatedPermission) permission).disaggregate().forEach(perm -> setAclPermission(acl, sid, perm));
            } else {
                setAclPermission(acl, sid, permission);
            }
        });
    }

    @Override
    public void deleteAcl(final ObjectIdentity identity) {
        _aclService.deleteAcl(identity, false);
    }

    @Override
    public <T extends EntityNode> void deleteAcl(final Class<T> entityClass, final long id) {
        deleteAcl(new ObjectIdentityImpl(entityClass.getName(), id));
    }

    private void setAclPermission(final MutableAcl acl, final Sid sid, final Permission permission) {
        acl.insertAce(acl.getEntries().size(), permission, sid, true);
        _aclService.updateAcl(acl);
    }

    private <T extends EntityNode> void addPermissions(final UserPrincipal user, final T entity, final Role role) {
        final MutableAcl acl = getEntityAcl(entity);
        final Sid        sid = new PrincipalSid(user.getUsername());

        setPermissionsOnAcl(acl, sid, role.getPermissions());

        log.info("Added permissions for user {} to ACL for {}: {}", user.getUsername(), entity.toString(), StringUtils.join(role.getPermissions().disaggregate(), ", "));
    }

    private void setPermissionsOnAcl(final MutableAcl acl, final Sid sid, final AggregatedPermission permissions) {
        permissions.disaggregate().forEach(permission -> {
            acl.insertAce(acl.getEntries().size(), permission, sid, true);
            _aclService.updateAcl(acl);
        });
    }

    private static final String                    QUERY_ACL_SID            = "SELECT * FROM acl_sid";
    private static final String                    QUERY_ACL_CLASS          = "SELECT * FROM acl_class";
    private static final String                    QUERY_ACL_OBJ_IDENTITIES = "SELECT\n" +
                                                                              "  obj.id                 AS id,\n" +
                                                                              "  cl.class               AS entityClass,\n" +
                                                                              "  obj.object_id_identity AS entityId\n" +
                                                                              "FROM acl_object_identity obj\n" +
                                                                              "  LEFT JOIN acl_class cl ON cl.id = obj.object_id_class";
    private static final String                    QUERY_ACL_ENTRIES        = "SELECT\n" +
                                                                              "  entry.id               AS id,\n" +
                                                                              "  cl.class               AS entityClass,\n" +
                                                                              "  obj.object_id_identity AS entityId,\n" +
                                                                              "  entry.ace_order        AS aceOrder,\n" +
                                                                              "  sid.sid                AS sid,\n" +
                                                                              "  sid.principal          AS principal,\n" +
                                                                              "  entry.mask             AS mask\n" +
                                                                              "FROM acl_entry entry\n" +
                                                                              "  LEFT JOIN acl_object_identity obj ON obj.id = entry.acl_object_identity\n" +
                                                                              "  LEFT JOIN acl_class cl ON cl.id = obj.object_id_class\n" +
                                                                              "  LEFT JOIN acl_sid sid ON sid.id = entry.sid";
    private final        IdentityManagementService _idmService;
    private final        MutableAclService         _aclService;
    private final        JdbcTemplate              _template;
}
