package org.nrg.xnat.workflows.flowable.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.nrg.xnat.workflows.flowable.data.domain.Experiment;
import org.nrg.xnat.workflows.flowable.data.domain.Project;
import org.nrg.xnat.workflows.flowable.data.domain.Subject;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.nrg.xnat.workflows.flowable.services.WorkbenchDataService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Configuration
@Slf4j
public class InitConfig {
    @Bean
    public CommandLineRunner checkFlowable(final RepositoryService repositoryService, final RuntimeService runtimeService, final TaskService taskService) {
        // This is just a sanity check to show that the workflow engine is active and properly configured.
        return args -> {
            log.info("Number of process definitions:       {}", repositoryService.createProcessDefinitionQuery().count());
            log.info("Number of tasks:                     {}", taskService.createTaskQuery().count());
            runtimeService.startProcessInstanceByKey("oneTaskProcess");
            log.info("Number of tasks after process start: {}", taskService.createTaskQuery().count());
        };
    }

    @Bean
    @Transactional
    public CommandLineRunner initializeData(final IdentityManagementService idmService, final WorkbenchDataService dataService, final PlatformTransactionManager transactionManager) {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userSystem, userSystem, Arrays.asList(new SimpleGrantedAuthority(roleUser), new SimpleGrantedAuthority(roleAdministrator))));

        // Create some sample users in the user repository.
        return (String... args) -> new TransactionTemplate(transactionManager).execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                final UserPrincipal system = !idmService.isInitialized() ? idmService.initialize() : idmService.findUser("system");
                final Set<Role>     roles  = idmService.findRoles("fellow", "radiologist", "pi");
                if (roles.isEmpty()) {
                    idmService.createRoleAndTemplate(system, "fellow");
                    idmService.createRoleAndTemplate(system, "radiologist");
                    idmService.createRoleAndTemplate(system, "pi");
                }

                // Now test for admin users. If they exist, we assume everything's set up.
                if (!idmService.existsUser(userAdministrator)) {
                    final UserPrincipal administrator = idmService.createUser(system, userAdministrator, userDefaultPassword, roleAdministrator);
                    log.info("Created administrator principal {}.", administrator.getUsername());

                    final List<UserPrincipal> users = idmService.createUsers(system, Stream.of("fellow1", "fellow2", "fellow3", "radiologist1", "radiologist2", "radiologist3", "pi1", "pi2", "pi3")
                                                                                           .collect(Collectors.toMap(Function.identity(),
                                                                                                                     username -> ImmutablePair.of(userDefaultPassword, Collections.singletonList(username.replaceAll("\\d+$", ""))))));
                    log.info("Created {} regular user principals.", users.size());

                    IntStream.rangeClosed(1, 3).mapToObj(Integer::toString).forEach(projectNumber -> {
                        final UserPrincipal owner   = idmService.findUser("pi" + projectNumber);
                        final Project       project = dataService.saveProject(owner, new Project(projectNumber, "Project " + projectNumber));
                        dataService.addMember(owner, idmService.findUser("radiologist" + projectNumber), project);
                        dataService.addCollaborator(owner, idmService.findUser("fellow" + projectNumber), project);
                        log.info("Created project {} with owner {}", project.getName(), owner.getUsername());

                        IntStream.rangeClosed(1, 3).mapToObj(Integer::toString).forEach(subjectNumber -> {
                            final String  subjectLabel = projectNumber + "_" + subjectNumber;
                            final Subject subject      = dataService.saveSubject(owner, new Subject(subjectLabel, "Subject " + subjectLabel, project));
                            log.info("Created subject {} in project {} with owner {}", subject.getName(), project.getName(), owner.getUsername());
                            IntStream.rangeClosed(1, 3).mapToObj(Integer::toString).forEach(experimentNumber -> {
                                final String     experimentLabel = subjectLabel + "_" + experimentNumber;
                                final String     experimentName  = "Experiment " + experimentLabel;
                                final Experiment experiment      = dataService.saveExperiment(owner, new Experiment(experimentLabel, experimentName, subject));
                                log.info("Created experiment {} on subject {} in project {} with owner {}", experiment.getName(), subject.getName(), project.getName(), owner.getUsername());
                            });
                        });
                    });
                }
            }
        });
    }

    @Value("${flowable.idm.roles.system:system}")
    private String roleSystem;
    @Value("${flowable.idm.roles.administrator:administrator}")
    private String roleAdministrator;
    @Value("${flowable.idm.roles.user:user}")
    private String roleUser;
    @Value("${flowable.idm.users.system:system}")
    private String userSystem;
    @Value("${flowable.idm.users.administrator:admin}")
    private String userAdministrator;
    @Value("${flowable.idm.users.default-password:pw}")
    private String userDefaultPassword;
}
