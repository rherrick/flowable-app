package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.Role;

public interface CustomRoleRepository {
    <T extends EntityNode> Role getRoleForEntity(final String name, final T entity);

    <T extends EntityNode> Role getRoleForEntity(String name, Class<T> entityClass, long entityId);
}
