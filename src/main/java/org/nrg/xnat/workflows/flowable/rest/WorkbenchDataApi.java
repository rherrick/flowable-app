package org.nrg.xnat.workflows.flowable.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.nrg.xnat.workflows.flowable.data.domain.*;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.nrg.xnat.workflows.flowable.services.WorkbenchDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api("XNAT Workbench Data API")
@RestController
@RequestMapping("/data")
public class WorkbenchDataApi {
    @Autowired
    public WorkbenchDataApi(final WorkbenchDataService dataService, final IdentityManagementService idmService) {
        _dataService = dataService;
        _idmService = idmService;
    }

    @ApiOperation(value = "Get all projects.", response = Project.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Projects successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "projects", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostFilter("hasRole('administrator') OR hasPermission(filterObject.id, 'org.nrg.xnat.workflows.flowable.data.domain.Project', 'read')")
    public List<Project> getAllProjects() {
        return _dataService.getProjects();
    }

    @ApiOperation(value = "Get a specific project.", response = Project.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Project successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 404, message = "Requested project wasn't found."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "projects/{label}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostAuthorize("hasRole('administrator') OR hasPermission(returnObject.id, 'org.nrg.xnat.workflows.flowable.data.domain.Project', 'read')")
    public Project getProject(@PathVariable final String label) {
        return _dataService.getProject(label);
    }

    @ApiOperation(value = "Create a project.", response = Project.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Project successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PostMapping(path = "projects", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') OR hasRole('pi')")
    public Project createProject(@RequestBody final Project project) {
        final UserPrincipal user = getUser();
        return _dataService.saveProject(user, project);
    }

    private UserPrincipal getUser() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final String username  = principal instanceof UserDetails ? ((UserDetails) principal).getUsername() : principal.toString();
        return _idmService.findUser(username);
    }

    @ApiOperation(value = "Update a project.", response = Project.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Project successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 404, message = "Requested project wasn't found."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "projects/{label}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("label == project.label AND (hasRole('administrator') OR hasPermission(project.id, 'org.nrg.xnat.workflows.flowable.data.domain.Project', 'write'))")
    public Project updateProject(@PathVariable final String label, @RequestBody final Project project) {
        return _dataService.saveProject(getUser(), project);
    }

    @ApiOperation(value = "Delete a project.")
    @ApiResponses({@ApiResponse(code = 200, message = "Project successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 404, message = "Requested project wasn't found."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @DeleteMapping(path = "projects/{id}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') OR hasPermission(id, 'org.nrg.xnat.workflows.flowable.data.domain.Project', 'delete')")
    public void deleteProject(@PathVariable final long id) {
        _dataService.deleteProject(getUser(), id);
    }

    @ApiOperation(value = "Get all subjects.", response = Subject.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Subjects successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "subjects", produces = APPLICATION_JSON_VALUE)
    public List<Subject> getAllSubjects() {
        return _dataService.getSubjects();
    }

    @ApiOperation(value = "Get all experiments.", response = Experiment.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Experiments successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "experiments", produces = APPLICATION_JSON_VALUE)
    public List<Experiment> getAllExperiments() {
        return _dataService.getExperiments();
    }

    @ApiOperation(value = "Get all experimentAssessors.", response = ExperimentAssessor.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "ExperimentAssessors successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "experimentAssessors", produces = APPLICATION_JSON_VALUE)
    public List<ExperimentAssessor> getAllExperimentAssessors() {
        return _dataService.getExperimentAssessors();
    }

    @ApiOperation(value = "Get all tags.", response = Tag.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Tags successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "tags", produces = APPLICATION_JSON_VALUE)
    public List<Tag> getAllTags() {
        return _dataService.getTags();
    }

    @ApiOperation(value = "Get all projects.", response = Project.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Projects successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "tree", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostFilter("hasRole('administrator') OR hasPermission(filterObject.id, filterObject.type, 'read')")
    public List<Project> getProjectTree() {
        return _dataService.getProjects();
    }

    @ApiOperation(value = "Get all children of the specified entity node.", response = EntityNode.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Children successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "tree/{type}/{id}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostFilter("hasRole('administrator') OR hasPermission(filterObject.id, filterObject.type, 'read')")
    public List<? extends EntityNode> getNodeTree(@PathVariable final String type, @PathVariable final long id) {
        return _dataService.getNodeChildren(type, id);
    }
    private final WorkbenchDataService      _dataService;
    private final IdentityManagementService _idmService;
}
