package org.nrg.xnat.workflows.flowable.delegates;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

@SuppressWarnings("unused")
@Slf4j
public class RequestDeclinedDelegate implements JavaDelegate {
    @Override
    public void execute(final DelegateExecution execution) {
        log.info("Handling a request declined execution call: {}", execution.getId());
    }
}
