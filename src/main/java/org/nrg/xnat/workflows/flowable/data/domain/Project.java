package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Project extends EntityNode {
    public Project(@NotNull final String label) {
        this(label, null, null);
    }
    public Project(@NotNull final String label, final String name, final Project parent) {
        super(label, name);
        setParent(parent);
    }
    public Project(@NotNull final String label, final String name) {
        this(label, name, null);
    }
    public Project(@NotNull final String label, final Project parent) {
        this(label, null, parent);
    }

    @Override
    public String toString() {
        return "Project " + getName();
    }

    // @OneToMany(mappedBy="parent")
    // private Collection<Project> children;

    // @OneToMany(mappedBy = "project")
    // private Collection<Subject> subjects;
    @ManyToOne
    private Project parent;
    /**
     * Contains a description for the project. This may include Markdown-formatted text.
     */
    @Size(max = EntityNode.MAX_DESCRIPTION)
    @Pattern(regexp = "^[\\p{Graph}\\p{Space}]*$", message = "The project description may be left empty, but if not can contain up to 255 characters.")
    @Column(length = EntityNode.MAX_DESCRIPTION)
    private String  description;
    private Date    created;
    private Date    updated;
    /**
     * Contains data specific to the project as JSON.
     */
    //@Type(type = "com.marvinformatics.hibernate.json.JsonUserType")
    //private JsonNode data;
    private String  data;
}
