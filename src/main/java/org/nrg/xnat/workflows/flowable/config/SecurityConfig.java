package org.nrg.xnat.workflows.flowable.config;

import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.RoleRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.RoleTemplateRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.UserGroupRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.UserRepository;
import org.nrg.xnat.workflows.flowable.services.impl.hibernate.JpaIdentityManagementServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    public SecurityConfig(final UserRepository users, final UserGroupRepository groups, final RoleTemplateRepository roleTemplates, final RoleRepository roles) {
        _users = users;
        _groups = groups;
        _roleTemplates = roleTemplates;
        _roles = roles;
    }

    @SuppressWarnings("deprecation")
    @Bean
    public PasswordEncoder encoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsServiceBean());
    }

    @Primary
    @Bean
    public UserDetailsService userDetailsServiceBean() {
        return new JpaIdentityManagementServiceImpl(_users, _groups, _roleTemplates, _roles);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/", "/static/**/*", "/styles/**/*", "/js/**/*", "/webjars/**/*").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/login")
            .permitAll()
            .and()
            .logout()
            .permitAll();
        http.authorizeRequests()
            .antMatchers("/console/**")
            .hasRole("administrator");
        http.headers().frameOptions().disable();
        http.httpBasic();
        http.csrf().disable();
    }
    private final UserRepository         _users;
    private final UserGroupRepository    _groups;
    private final RoleTemplateRepository _roleTemplates;
    private final RoleRepository         _roles;
}
