package org.nrg.xnat.workflows.flowable.data.security;

import lombok.*;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserPrincipal extends BaseMetadata implements UserDetails, Serializable {
    public UserPrincipal(final String username, final String password) {
        this(username, password, null, null, null, null);
    }
    public UserPrincipal(final String username, final String password, final String fullName, final String email, final Set<Role> roles, final Set<UserGroup> groups) {
        this(username, password, fullName, email, roles, groups, true, true, true, true, false);
    }
    public UserPrincipal(final String username, final String password, final String fullName, final String email) {
        this(username, password, fullName, email, null, null);
    }
    public UserPrincipal(final String username, final String password, final Set<Role> roles, final Set<UserGroup> groups) {
        this(username, password, null, null, roles, groups);
    }

    /**
     * The user's authorities depends on a combination of individual {@link Role roles} and the roles inherited from any
     * {@link UserGroup groups} to which the user belongs.
     *
     * @return A list of the authorities (in the form of {@link Role roles}) the user possesses.
     */
    @Transient
    @Override
    public Collection<Role> getAuthorities() {
        return Stream.concat(roles.stream(), groups.stream().map(UserGroup::getRoles).flatMap(Set::stream)).distinct().collect(Collectors.toList());
    }

    public boolean addRole(final Role role) {
        return roles.add(role);
    }

    public boolean removeRole(final Role role) {
        return roles.remove(role);
    }

    public boolean addGroup(final UserGroup group) {
        return groups.add(group);
    }

    public boolean removeGroup(final UserGroup group) {
        return groups.remove(group);
    }

    @Override
    public String toString() {
        return getUsername();
    }
    @Column(nullable = false, unique = true)
    private String         username;
    private String         password;
    private String         fullName;
    private String         email;
    @ManyToMany(fetch = FetchType.EAGER)
    @Builder.Default
    private Set<Role>      roles                 = new HashSet<>();
    @ManyToMany(fetch = FetchType.EAGER)
    @Builder.Default
    private Set<UserGroup> groups                = new HashSet<>();
    @Builder.Default
    private boolean        enabled               = true;
    @Builder.Default
    private boolean        credentialsNonExpired = true;
    @Builder.Default
    private boolean        accountNonLocked      = true;
    @Builder.Default
    private boolean        accountNonExpired     = true;
    @Builder.Default
    private boolean        systemUser            = false;
}