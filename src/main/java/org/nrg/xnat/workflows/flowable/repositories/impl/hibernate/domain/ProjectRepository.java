package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain;

import org.nrg.xnat.workflows.flowable.data.domain.Project;
import org.nrg.xnat.workflows.flowable.data.domain.Tag;
import org.nrg.xnat.workflows.flowable.repositories.EntityNodeRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ProjectRepository extends EntityNodeRepository<Project> {
    List<Project> findAllByParent(final Project project);

    List<Project> findByType(final String type);

    List<Project> findByTags(final Tag... tags);

    Project findByLabel(final String label);

    Project findByName(final String name);
}
