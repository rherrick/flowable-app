package org.nrg.xnat.workflows.flowable.data.security;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.nrg.xnat.workflows.flowable.data.AggregatedPermission;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;
import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Specifies a user role. A role may be defined as a general user category (e.g. administrator) or as an entity-specific
 * set of permissions (e.g. owner of a project). The permissions associated with the role can be pre-defined by setting
 * the value of the {@link #setPermissions(Permission) permissions property}. An associated object can be specified by
 * setting the {@link #setEntityClass(String)} associated object property}.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name", "entityClass", "entityId"}))
@Getter
@Setter
public class Role extends BaseMetadata implements GrantedAuthority {
    @SuppressWarnings("unused")
    public Role() {
        setPermissions(new AggregatedPermission());
    }
    public Role(final RoleTemplate template) {
        setName(template.getName());
        setPermissions(template.getPermissions());
    }
    public <T extends EntityNode> Role(final RoleTemplate template, final T entity) {
        this(template, entity.getClass().getName(), entity.getId());
    }
    public Role(final RoleTemplate template, final String entityClass, final long entityId) {
        setName(template.getName());
        setPermissions(template.getPermissions());
        setEntityClass(entityClass);
        setEntityId(entityId);
    }

    /**
     * Returns the authority for this role. For general roles, this is just the role name, while entity-specific roles
     * will include the object identity.
     *
     * @return The authority for this role.
     */
    @Override
    @Transient
    public String getAuthority() {
        return getName();
    }

    /**
     * Sets the {@link #setEntityClass(String)} and {@link #setEntityId(long)} properties from the class and ID of the
     * submitted entity instance.
     *
     * @param entity The entity instance to set for this role.
     * @param <T>    The type of the {@link EntityNode}-based class.
     */
    public <T extends EntityNode> void setEntity(final T entity) {
        setEntityClass(entity.getClass().getName());
        setEntityId(entity.getId());
    }

    /**
     * Gets the permissions for this role.
     */
    @Transient
    public AggregatedPermission getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions for this role. Note that this completely replaces any existing permissions settings.
     *
     * @param permissions The permissions to set for the role.
     */
    public void setPermissions(final Permission permissions) {
        this.permissions.clear();
        this.permissions.set(permissions);
        mask = this.permissions.getMask();
    }

    /**
     * Adds the specific permission(s) to the permissions already set for this role.
     *
     * @param permissions The permission(s) to add for this role.
     */
    public void addPermissions(final Permission permissions) {
        this.permissions.set(permissions);
        mask = permissions.getMask();
    }

    /**
     * Removes the permission specified in the submitted parameter from the permissions set for this role.
     *
     * @param permissions The permissions to remove from this role.
     */
    public void removePermissions(final Permission permissions) {
        this.permissions.clear(permissions);
        mask = permissions.getMask();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getName())
                .append(getMask())
                .append(getEntityClass())
                .append(getEntityId())
                .toHashCode();
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof Role)) {
            return false;
        }

        final Role role = (Role) object;

        return new EqualsBuilder()
                .append(getName(), role.getName())
                .append(getMask(), role.getMask())
                .append(getEntityClass(), role.getEntityClass())
                .append(getEntityId(), role.getEntityId())
                .isEquals();
    }

    @Override
    public String toString() {
        return StringUtils.isBlank(getEntityClass()) ? getName() : getName() + " " + getEntityClass() + " " + getEntityId();
    }
    private final transient AggregatedPermission permissions = new AggregatedPermission();
    /**
     * The name of the role.
     */
    @NonNull
    @Column(nullable = false)
    private                 String               name;
    /**
     * The default permissions to set for the role in the form of a bitmask.
     */
    private                 int                  mask;
    /**
     * The class of the object with which this instance of this role is associated. This corresponds directly to the
     * <b>class</b> column of the Spring Security <b>acl_class</b> table. The associated object is referenced in full by
     * the <b>object_id_class</b> foreign key and the <b>object_id_identity</b> key in the <b>acl_object_identity</b>
     * table, which indicates the type and specific ID of the entity.
     */
    @NonNull
    private                 String               entityClass;
    /**
     * The instance ID of the object with which this instance of this role is associated. This corresponds directly to
     * the <b>object_id_identity</b> key in the <b>acl_object_identity</b> table.
     */
    private                 long                 entityId;
}
