package org.nrg.xnat.workflows.flowable.data;

import org.springframework.security.acls.domain.AbstractPermission;

public class SingularPermission extends AbstractPermission {
    SingularPermission(final int mask) {
        super(mask);
    }
}
