package org.nrg.xnat.workflows.flowable.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.data.security.RoleTemplate;
import org.nrg.xnat.workflows.flowable.data.security.UserGroup;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(description = "Identify Management API")
@RestController
@RequestMapping("/idm")
public class IdentityManagementApi {
    @Autowired
    public IdentityManagementApi(final IdentityManagementService service) {
        _service = service;
    }

    @ApiOperation(value = ".", notes = ".", response = UserPrincipal.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "users", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    public List<UserPrincipal> getUsers() {
        return _service.getAllUsers();
    }

    @ApiOperation(value = ".", notes = ".", response = UserPrincipal.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "users/{username}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') OR username == authentication.name")
    public UserPrincipal getUser(@PathVariable final String username) {
        return _service.findUser(username);
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "User successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PostMapping(path = "users", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean createUser(@RequestBody final UserPrincipal user) {
        _service.saveUser(getUser(), user);
        return true;
    }

    private UserPrincipal getUser() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final String username  = principal instanceof UserDetails ? ((UserDetails) principal).getUsername() : principal.toString();
        return _service.findUser(username);
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "users/{username}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') OR (username == authentication.name AND username == user.username)")
    public boolean updateUser(@PathVariable final String username, @RequestBody final UserPrincipal user) {
        _service.saveUser(getUser(), user);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @DeleteMapping(path = "users/{username}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean deleteUser(@PathVariable final String username) {
        _service.deleteUser(getUser(), username);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = UserGroup.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "users/{username}/groups", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') or username == authentication.name")
    public Set<UserGroup> getGroupsForUser(@PathVariable final String username) {
        return _service.findUser(username).getGroups();
    }

    @ApiOperation(value = ".", notes = ".", response = UserGroup.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "users/{username}/groups/{group}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') or username == authentication.name")
    public Optional<UserGroup> getGroupForUser(@PathVariable final String username, @PathVariable final String group) {
        return _service.findUser(username).getGroups().stream().filter(userGroup -> group.equals(userGroup.getName())).findFirst();
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 404, message = "One of the requested entities could not be found."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "users/{username}/groups/{group}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean addGroupForUser(@PathVariable final String username, @PathVariable final String group) {
        final UserPrincipal user = _service.findUser(username);
        if (user == null) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "No user found for " + username);
        }
        final UserGroup userGroup = _service.findUserGroup(group);
        if (userGroup == null) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "No group found for " + username);
        }
        if (user.getGroups().contains(userGroup)) {
            throw new HttpClientErrorException(HttpStatus.NO_CONTENT, "The user " + username + " is already associated with the user group " + group);
        }
        user.getGroups().add(userGroup);
        _service.saveUser(getUser(), user);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = UserGroup.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "groups", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostFilter("hasRole('administrator') OR hasRole(filterObject.name)")
    public List<UserGroup> getUserGroups() {
        return _service.getAllUserGroups();
    }

    @ApiOperation(value = ".", notes = ".", response = UserGroup.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "groups/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('user')")
    @PostAuthorize("hasRole('administrator') OR hasRole(returnObject.name)")
    public UserGroup getUserGroup(@PathVariable final String name) {
        return _service.findUserGroup(name);
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "groups/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean updateUserGroup(@PathVariable final String name, @RequestBody final UserGroup group) {
        _service.saveUserGroup(getUser(), group);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @DeleteMapping(path = "groups/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean deleteUserGroup(@PathVariable final String name) {
        _service.deleteUserGroup(getUser(), name);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = Role.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "roles", produces = APPLICATION_JSON_VALUE)
    public List<Role> getRoles() {
        return _service.getAllRoles();
    }

    @ApiOperation(value = ".", notes = ".", response = Role.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "roles/{name}", produces = APPLICATION_JSON_VALUE)
    public Role getRole(@PathVariable final String name) {
        return _service.findRole(name);
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "roles/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') AND role.name == name")
    public boolean updateRole(@PathVariable final String name, @RequestBody final Role role) {
        _service.saveRole(getUser(), role);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @DeleteMapping(path = "roles/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean deleteRole(@PathVariable final String name) {
        _service.deleteRole(getUser(), name);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = RoleTemplate.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "templates", produces = APPLICATION_JSON_VALUE)
    public List<RoleTemplate> getRoleTemplates() {
        return _service.getAllRoleTemplates();
    }

    @ApiOperation(value = ".", notes = ".", response = RoleTemplate.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @GetMapping(path = "templates/{name}", produces = APPLICATION_JSON_VALUE)
    public RoleTemplate getRoleTemplate(@PathVariable final String name) {
        return _service.findRoleTemplate(name);
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @PutMapping(path = "templates/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator') AND roleTemplate.name == name")
    public boolean updateRoleTemplate(@PathVariable final String name, @RequestBody final RoleTemplate roleTemplate) {
        _service.saveRoleTemplate(getUser(), roleTemplate);
        return true;
    }

    @ApiOperation(value = ".", notes = ".", response = boolean.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Requested items successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT Workbench REST API."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @DeleteMapping(path = "templates/{name}", produces = APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('administrator')")
    public boolean deleteRoleTemplate(@PathVariable final String name) {
        _service.deleteRoleTemplate(getUser(), name);
        return true;
    }
    private final IdentityManagementService _service;
}
