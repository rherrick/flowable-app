package org.nrg.xnat.workflows.flowable.services;

import org.apache.commons.lang3.tuple.Pair;
import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.data.security.RoleTemplate;
import org.nrg.xnat.workflows.flowable.data.security.UserGroup;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.springframework.security.acls.model.Permission;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public interface IdentityManagementService {
    <T extends EntityNode> void createStandardEntityRoles(final UserPrincipal actor, final T entity);

    <T extends EntityNode> Role getOwnerRole(final T entity);

    <T extends EntityNode> Role getMemberRole(final T entity);

    <T extends EntityNode> Role getCollaboratorRole(final T entity);

    boolean existsUser(final String username);

    UserPrincipal findUser(final String username);

    List<UserPrincipal> findUsers(final String... users);

    List<UserPrincipal> findUsers(List<String> users);

    UserGroup findUserGroup(final String name);

    List<UserGroup> findUserGroups(final String... names);

    List<UserGroup> findUserGroups(final List<String> names);

    RoleTemplate findRoleTemplate(String role);

    Set<RoleTemplate> findRoleTemplates(String... roles);

    Set<RoleTemplate> findRoleTemplates(List<String> roles);

    Role findRole(final String role);

    <T extends EntityNode> Role findRole(final String role, final T entity);

    <T extends EntityNode> Role findRole(final String role, final Class<T> entityClass, final long entityId);

    Set<Role> findRoles(String... roles);

    Set<Role> findRoles(List<String> roles);

    List<UserPrincipal> getAllUsers();

    List<UserGroup> getAllUserGroups();

    List<Role> getAllRoles();

    List<RoleTemplate> getAllRoleTemplates();

    UserPrincipal createUser(final UserPrincipal actor, final String username, final String password, final String... roles);

    UserPrincipal createUser(UserPrincipal actor, String username, String password, List<String> roles);

    /**
     * Batch createUsers method that takes a map of the basic user information. The map key contains the username, while the
     * value contains a pair of values, with the left value containing the password and the right value containing a
     * list of the roles to set for the user.
     *
     * @param actor                      The user performing the createUsers operation.
     * @param usernamesPasswordsAndRoles The map of user data for the users to be created.
     *
     * @return A list of the newly created users.
     */
    List<UserPrincipal> createUsers(UserPrincipal actor, Map<String, Pair<String, List<String>>> usernamesPasswordsAndRoles);

    UserPrincipal saveUser(final UserPrincipal actor, final UserPrincipal user);

    void deleteUser(final UserPrincipal actor, final String username);

    void deleteUser(UserPrincipal actor, long id);

    void deleteUser(final UserPrincipal actor, final UserPrincipal user);

    UserGroup createUserGroup(final UserPrincipal actor, final String name, final String... roles);

    UserGroup createUserGroup(final UserPrincipal actor, final String name, final List<String> roles);

    Map<String, UserGroup> createUserGroups(final UserPrincipal actor, final String... names);

    Map<String, UserGroup> createUserGroups(final UserPrincipal actor, final List<String> names);

    Map<String, UserGroup> createUserGroups(final UserPrincipal actor, Map<String, List<String>> userGroupsAndRoles);

    UserGroup saveUserGroup(final UserPrincipal actor, final UserGroup group);

    void deleteUserGroup(final UserPrincipal actor, final String name);

    void deleteUserGroup(UserPrincipal actor, long id);

    void deleteUserGroup(final UserPrincipal actor, final UserGroup group);

    RoleTemplate createRoleTemplate(final UserPrincipal actor, final String name, final boolean requireAssociatedObject);

    RoleTemplate createRoleTemplate(final UserPrincipal actor, final String name, final boolean requireAssociatedObject, final Permission permissions);

    RoleTemplate saveRoleTemplate(final UserPrincipal actor, final RoleTemplate roleTemplate);

    void deleteRoleTemplate(final UserPrincipal actor, final String roleTemplate);

    void deleteRoleTemplate(UserPrincipal actor, long id);

    void deleteRoleTemplate(final UserPrincipal actor, final RoleTemplate roleTemplate);

    Role createRoleAndTemplate(final UserPrincipal actor, final String name);

    Map<String, Role> createRolesAndTemplates(final UserPrincipal actor, final List<String> names);

    Optional<Role> createRole(final UserPrincipal actor, final String name);

    Optional<Role> createRole(final UserPrincipal actor, final String name, final EntityNode node);

    Map<String, Optional<Role>> createRoles(final UserPrincipal actor, final String... names);

    Map<String, Optional<Role>> createRoles(final UserPrincipal actor, final List<String> names);

    @SuppressWarnings("UnusedReturnValue")
    Role saveRole(final UserPrincipal actor, final Role role);

    void deleteRole(final UserPrincipal actor, final String role);

    void deleteRole(UserPrincipal actor, long id);

    void deleteRole(final UserPrincipal actor, final Role role);

    boolean isInitialized();

    UserPrincipal initialize();
}
