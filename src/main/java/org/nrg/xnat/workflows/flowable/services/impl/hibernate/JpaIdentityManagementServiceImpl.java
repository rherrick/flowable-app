package org.nrg.xnat.workflows.flowable.services.impl.hibernate;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nrg.xnat.workflows.flowable.data.AggregatedPermission;
import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.data.security.RoleTemplate;
import org.nrg.xnat.workflows.flowable.data.security.UserGroup;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.RoleRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.RoleTemplateRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.UserGroupRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security.UserRepository;
import org.nrg.xnat.workflows.flowable.services.AbstractWorkbenchService;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.nrg.xnat.workflows.flowable.data.AggregatedPermission.*;

@Transactional
@Service
@Slf4j
public class JpaIdentityManagementServiceImpl extends AbstractWorkbenchService implements UserDetailsService, IdentityManagementService {
    @Autowired
    public JpaIdentityManagementServiceImpl(final UserRepository users, final UserGroupRepository groups, final RoleTemplateRepository roleTemplates, final RoleRepository roles) {
        _users = users;
        _groups = groups;
        _roleTemplates = roleTemplates;
        _roles = roles;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final UserPrincipal user = _users.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

    @Override
    public <T extends EntityNode> void createStandardEntityRoles(final UserPrincipal actor, final T entity) {
        groupRoles.forEach(role -> createRole(actor, role, entity));
    }

    @Override
    public <T extends EntityNode> Role getOwnerRole(final T entity) {
        return _roles.getRoleForEntity(groupRoles.get(0), entity);
    }

    @Override
    public <T extends EntityNode> Role getMemberRole(final T entity) {
        return _roles.getRoleForEntity(groupRoles.get(1), entity);
    }

    @Override
    public <T extends EntityNode> Role getCollaboratorRole(final T entity) {
        return _roles.getRoleForEntity(groupRoles.get(2), entity);
    }

    @Override
    public boolean existsUser(final String username) {
        return _users.existsByUsername(username);
    }

    @Override
    public UserPrincipal findUser(final String username) {
        return _users.findByUsername(username);
    }

    @Override
    public List<UserPrincipal> findUsers(final String... users) {
        return findUsers(Arrays.asList(users));
    }

    @Override
    public List<UserPrincipal> findUsers(final List<String> users) {
        return _users.findAllByUsernameIn(users);
    }

    @Override
    public UserGroup findUserGroup(final String name) {
        return _groups.findByName(name);
    }

    @Override
    public List<UserGroup> findUserGroups(final String... names) {
        return findUserGroups(Arrays.asList(names));
    }

    @Override
    public List<UserGroup> findUserGroups(final List<String> names) {
        return _groups.findAllByNameIn(names);
    }

    @Override
    public RoleTemplate findRoleTemplate(final String role) {
        return _roleTemplates.findByName(role);
    }

    @Override
    public Set<RoleTemplate> findRoleTemplates(final String... roles) {
        return findRoleTemplates(Arrays.asList(roles));
    }

    @Override
    public Set<RoleTemplate> findRoleTemplates(final List<String> roles) {
        return new HashSet<>(_roleTemplates.findAllByNameIn(roles));
    }

    @Override
    public Role findRole(final String role) {
        return _roles.findByName(role);
    }

    @Override
    public <T extends EntityNode> Role findRole(final String role, final T entity) {
        return _roles.getRoleForEntity(role, entity);
    }

    @Override
    public <T extends EntityNode> Role findRole(final String role, final Class<T> entityClass, final long entityId) {
        return _roles.getRoleForEntity(role, entityClass, entityId);
    }

    @Override
    public Set<Role> findRoles(final String... roles) {
        return findRoles(Arrays.asList(roles));
    }

    @Override
    public Set<Role> findRoles(final List<String> roles) {
        return new HashSet<>(_roles.findAllByNameIn(roles));
    }

    @Override
    public List<UserPrincipal> getAllUsers() {
        return iterableToList(_users.findAll());
    }

    @Override
    public List<UserGroup> getAllUserGroups() {
        return iterableToList(_groups.findAll());
    }

    @Override
    public List<Role> getAllRoles() {
        return iterableToList(_roles.findAll());
    }

    @Override
    public List<RoleTemplate> getAllRoleTemplates() {
        return iterableToList(_roleTemplates.findAll());
    }

    @Override
    public UserPrincipal createUser(final UserPrincipal actor, final String username, final String password, final String... roles) {
        return createUser(actor, username, password, Arrays.asList(roles));
    }

    @Override
    public UserPrincipal createUser(final UserPrincipal actor, final String username, final String password, final List<String> roles) {
        final UserPrincipal user = saveUser(actor, UserPrincipal.builder().username(username).password(password).roles(findRolesWithUser(roles)).build());
        log.info("Created user {} with role {}", username, roleAdministrator);
        return user;
    }

    @Override
    public List<UserPrincipal> createUsers(final UserPrincipal actor, final Map<String, Pair<String, List<String>>> usernamesPasswordsAndRoles) {
        List<UserPrincipal> users = usernamesPasswordsAndRoles.entrySet()
                                                              .stream()
                                                              .map(entry -> createUser(actor, entry.getKey(), entry.getValue().getLeft(), entry.getValue().getRight()))
                                                              .collect(Collectors.toList());
        if (log.isInfoEnabled()) {
            log.info("Created users {} with roles {}",
                     StringUtils.join(users.stream().map(UserPrincipal::getUsername).collect(Collectors.toList()), ", "),
                     StringUtils.join(users.stream().map(UserPrincipal::getAuthorities).collect(Collectors.toSet()), ", "));
        }
        return users;
    }

    @Override
    public UserPrincipal saveUser(final UserPrincipal actor, final UserPrincipal user) {
        return _users.save(user);
    }

    @Override
    public void deleteUser(final UserPrincipal actor, final String username) {
        _users.delete(_users.findByUsername(username));
    }

    @Override
    public void deleteUser(final UserPrincipal actor, final long id) {
        _users.delete(_users.findById(id).orElseThrow(() -> new EntityNotFoundException("No user found with ID " + id)));
    }

    @Override
    public void deleteUser(final UserPrincipal actor, final UserPrincipal user) {
        _users.delete(user);
    }

    @Override
    public UserGroup createUserGroup(final UserPrincipal actor, final String name, final String... roles) {
        return createUserGroup(actor, name, Arrays.asList(roles));
    }

    @Override
    public UserGroup createUserGroup(final UserPrincipal actor, final String groupName, final List<String> roleNames) {
        final Map<Boolean, List<String>> roleMap = roleNames.stream().collect(Collectors.groupingBy(_roles::existsByName));
        final Set<Role>                  roles   = new HashSet<>();
        if (roleMap.containsKey(true)) {
            roles.addAll(roleMap.get(true).stream().map(_roles::findByName).collect(Collectors.toSet()));
        }
        if (roleMap.containsKey(false)) {
            roles.addAll(roleMap.get(false).stream().map(name -> createRoleAndTemplate(actor, name)).collect(Collectors.toSet()));
        }

        final UserGroup group = new UserGroup(groupName);
        group.setRoles(roles);
        _groups.save(group);
        return group;
    }

    @Override
    public Map<String, UserGroup> createUserGroups(final UserPrincipal actor, final String... names) {
        return createUserGroups(actor, Arrays.asList(names));
    }

    @Override
    public Map<String, UserGroup> createUserGroups(final UserPrincipal actor, final List<String> names) {
        return names.stream().collect(Collectors.toMap(Function.identity(), name -> createUserGroup(actor, name)));
    }

    @Override
    public Map<String, UserGroup> createUserGroups(final UserPrincipal actor, final Map<String, List<String>> userGroupsAndRoles) {
        return userGroupsAndRoles.entrySet()
                                 .stream()
                                 .collect(Collectors.toMap(Map.Entry::getKey,
                                                           entry -> createUserGroup(actor, entry.getKey(), entry.getValue())));
    }

    @Override
    public UserGroup saveUserGroup(final UserPrincipal actor, final UserGroup group) {
        return _groups.save(group);
    }

    @Override
    public void deleteUserGroup(final UserPrincipal actor, final String name) {
        if (_groups.existsByName(name)) {
            _groups.delete(_groups.findByName(name));
        }
    }

    @Override
    public void deleteUserGroup(final UserPrincipal actor, final long id) {
        _groups.deleteById(id);
    }

    @Override
    public void deleteUserGroup(final UserPrincipal actor, final UserGroup group) {
        _groups.delete(group);
    }

    @Override
    public RoleTemplate createRoleTemplate(final UserPrincipal actor, final String name, final boolean requireAssociatedObject) {
        return createRoleTemplate(actor, name, requireAssociatedObject, null);
    }

    @Override
    public RoleTemplate createRoleTemplate(final UserPrincipal actor, final String name, final boolean requireAssociatedObject, final Permission permissions) {
        final RoleTemplate roleTemplate = permissions == null ? new RoleTemplate(name, requireAssociatedObject) : new RoleTemplate(name, requireAssociatedObject, permissions);
        _roleTemplates.save(roleTemplate);
        return roleTemplate;
    }

    @Override
    public RoleTemplate saveRoleTemplate(final UserPrincipal actor, final RoleTemplate roleTemplate) {
        return _roleTemplates.save(roleTemplate);
    }

    @Override
    public void deleteRoleTemplate(final UserPrincipal actor, final String roleTemplate) {
        _roleTemplates.delete(_roleTemplates.findByName(roleTemplate));
    }

    @Override
    public void deleteRoleTemplate(final UserPrincipal actor, final long id) {
        _roleTemplates.delete(_roleTemplates.findById(id).orElseThrow(() -> new EntityNotFoundException("No roleTemplate found with ID " + id)));
    }

    @Override
    public void deleteRoleTemplate(final UserPrincipal actor, final RoleTemplate roleTemplate) {
        _roleTemplates.delete(roleTemplate);
    }

    @Override
    public Role createRoleAndTemplate(final UserPrincipal actor, final String name) {
        if (!_roleTemplates.existsByName(name)) {
            createRoleTemplate(actor, name, false);
        }
        final Optional<Role> role = createRole(actor, name);
        return role.orElse(null);
    }

    @Override
    public Map<String, Role> createRolesAndTemplates(final UserPrincipal actor, final List<String> names) {
        return names.stream().collect(Collectors.toMap(Function.identity(), role -> createRoleAndTemplate(actor, role)));
    }

    @Override
    public Optional<Role> createRole(final UserPrincipal actor, final String name) {
        return createRole(actor, name, null);
    }

    @Override
    public Optional<Role> createRole(final UserPrincipal actor, final String name, final EntityNode entity) {
        if (!_roleTemplates.existsByName(name)) {
            return Optional.empty();
        }

        final RoleTemplate template = _roleTemplates.findByName(name);
        if (template.isRequireAssociatedObject() && entity == null) {
            throw new RuntimeException("Roles created from the role template " + name + " require an associated object, but none was specified.");
        }

        final Role role;
        if (entity != null) {
            role = new Role(template, entity);
        } else {
            role = new Role(template);
        }
        saveRole(actor, role);
        return Optional.of(role);
    }

    @Override
    public Map<String, Optional<Role>> createRoles(final UserPrincipal actor, final String... names) {
        return createRoles(actor, Arrays.asList(names));
    }

    @Override
    public Map<String, Optional<Role>> createRoles(final UserPrincipal actor, final List<String> names) {
        return names.stream().collect(Collectors.toMap(Function.identity(), name -> createRole(actor, name)));
    }

    @Override
    public Role saveRole(final UserPrincipal actor, final Role role) {
        return _roles.save(role);
    }

    @Override
    public void deleteRole(final UserPrincipal actor, final String role) {
        _roles.delete(_roles.findByName(role));
    }

    @Override
    public void deleteRole(final UserPrincipal actor, final long id) {
        _roles.delete(_roles.findById(id).orElseThrow(() -> new EntityNotFoundException("No role found with ID " + id)));
    }

    @Override
    public void deleteRole(final UserPrincipal actor, final Role role) {
        _roles.delete(role);
    }

    @Override
    public boolean isInitialized() {
        final List<Role> defaultRoles = _roles.findAllByNameIn(Arrays.asList(roleUser, roleAdministrator, roleSystem));
        return _users.findByUsername(userSystem) != null && defaultRoles != null && defaultRoles.size() == 3;
    }

    @PostConstruct
    @Override
    public UserPrincipal initialize() {
        final RoleTemplate systemRoleTemplate = _roleTemplates.existsByName(roleSystem) ? _roleTemplates.findByName(roleSystem) : _roleTemplates.save(new RoleTemplate(roleSystem, false, SYSTEM_PERMISSIONS));
        final RoleTemplate adminRoleTemplate  = _roleTemplates.existsByName(roleAdministrator) ? _roleTemplates.findByName(roleAdministrator) : _roleTemplates.save(new RoleTemplate(roleAdministrator, false, ADMIN_PERMISSIONS));
        final RoleTemplate userRoleTemplate   = _roleTemplates.existsByName(roleUser) ? _roleTemplates.findByName(roleUser) : _roleTemplates.save(new RoleTemplate(roleUser, false));

        IntStream.range(0, groupRoles.size()).boxed().collect(Collectors.toMap(groupRoles::get, permissions::get)).forEach((groupRole, value) -> {
            final int topPermission = Integer.parseInt(value);

            // Create standard role templates.
            if (!_roleTemplates.existsByName(groupRole)) {
                final AggregatedPermission permission = new AggregatedPermission();
                IntStream.rangeClosed(0, topPermission).forEach(index -> permission.set(BASE_PERMISSIONS[index]));
                _roleTemplates.save(new RoleTemplate(groupRole, true, permission));
            }
        });

        final Set<Role> roles = new HashSet<>(Arrays.asList(_roles.existsByName(roleSystem) ? _roles.findByName(roleSystem) : _roles.save(new Role(systemRoleTemplate)),
                                                            _roles.existsByName(roleAdministrator) ? _roles.findByName(roleAdministrator) : _roles.save(new Role(adminRoleTemplate)),
                                                            _roles.existsByName(roleUser) ? _roles.findByName(roleUser) : _roles.save(new Role(userRoleTemplate))));
        final UserPrincipal system = _users.existsByUsername(userSystem)
                                     ? _users.findByUsername(userSystem)
                                     : _users.save(UserPrincipal.builder().username(userSystem).password(userDefaultPassword).roles(roles).systemUser(true).build());
        log.info("Created user {} with roles {}", system.getUsername(), StringUtils.join(system.getAuthorities(), ", "));
        return system;
    }

    private Set<Role> findRolesWithUser(final List<String> roles) {
        final Set<Role> found = findRoles(roles);
        if (!roles.contains(roleUser)) {
            found.add(findRole(roleUser));
        }
        return found;
    }

    private final        UserRepository         _users;
    private final        UserGroupRepository    _groups;
    private final        RoleTemplateRepository _roleTemplates;
    private final        RoleRepository         _roles;
    @Value("${flowable.idm.users.system:system}")
    private              String                 userSystem;
    @Value("${flowable.idm.users.default-password:password}")
    private              String                 userDefaultPassword;
    @Value("${flowable.idm.roles.system:system}")
    private              String                 roleSystem;
    @Value("${flowable.idm.roles.administrator:administrator}")
    private              String                 roleAdministrator;
    @Value("${flowable.idm.roles.user:user}")
    private              String                 roleUser;
    @Value("#{'${flowable.idm.roles.groups.names:owner,member,collaborator}'.split(',')}")
    private              List<String>           groupRoles;
    @Value("#{'${flowable.idm.roles.groups.permissions:4,3,1}'.split(',')}")
    private              List<String>           permissions;

}
