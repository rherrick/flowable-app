package org.nrg.xnat.workflows.flowable.rest;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/workflows")
@Slf4j
public class WorkflowsApi {
    @SuppressWarnings({"SpringJavaAutowiringInspection"})
    @Autowired
    public WorkflowsApi(final RepositoryService repositoryService, final RuntimeService runtimeService, final TaskService taskService) {
        _repositoryService = repositoryService;
        _runtimeService = runtimeService;
        _taskService = taskService;
    }

    @GetMapping(path = "processes/{processId}", produces = APPLICATION_JSON_VALUE)
    public ProcessDefinition getWorkflow(@PathVariable final String processId) {
        return _repositoryService.getProcessDefinition(processId);
    }

    @PostMapping(path = "processes/{processId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public String launchWorkflow(@PathVariable final String processId, @RequestBody final Map<String, String> parameters) {
        final Map<String, Object> map      = parameters.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> (Object) entry.getValue()));
        final ProcessInstance     instance = _runtimeService.startProcessInstanceByKey("data-release-request", map);
        log.info("Just launched an instance of the process {}", processId);
        return instance.getProcessInstanceId();
    }

    @GetMapping(path = "tasks", produces = APPLICATION_JSON_VALUE)
    public List<Task> getTasks() {
        final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getTasks(user.getUsername());
    }

    @GetMapping(path = "tasks/assignee", produces = APPLICATION_JSON_VALUE)
    public List<Task> getTasks(@PathVariable final String assignee) {
        return _taskService.createTaskQuery().taskAssignee(assignee).list();
    }

    @GetMapping(path = "tasks/{taskId}", produces = APPLICATION_JSON_VALUE)
    public Task getTask(@PathVariable final String taskId) {
        return _taskService.createTaskQuery().processInstanceId(taskId).singleResult();
    }

    private final RepositoryService _repositoryService;
    private final RuntimeService    _runtimeService;
    private final TaskService       _taskService;
}
