package org.nrg.xnat.workflows.flowable.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import static javax.persistence.TemporalType.TIMESTAMP;

@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public abstract class BaseMetadata implements Serializable { // extends DefaultRevisionEntity { Consider extending org.hibernate.envers.DefaultRevisionEntity
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof BaseMetadata)) {
            return false;
        }
        if (!super.equals(object)) {
            return false;
        }
        final BaseMetadata that = (BaseMetadata) object;
        return getId() == that.getId();
    }
    @CreatedBy
    protected String createdBy;
    @CreatedDate
    @Temporal(TIMESTAMP)
    protected Date   created;
    @LastModifiedBy
    protected String lastModifiedBy;
    @LastModifiedDate
    @Temporal(TIMESTAMP)
    protected Date   lastModified;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private   long   id;
}
