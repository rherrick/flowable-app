package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Provides the base functionality for entity nodes.
 */
@MappedSuperclass
@Getter
@Setter
@RequiredArgsConstructor
public abstract class EntityNode extends BaseMetadata {
    public static final int      MAX_DESCRIPTION = 65535;
    EntityNode(@NotNull final String label) {
        this(label, label);
    }
    EntityNode(@NotNull final String label, @NotNull final String name) {
        Assert.hasText(label, "Entity labels must contain some readable text: null, empty, or blank labels can not be used.");
        setLabel(label);
        setName(StringUtils.defaultIfBlank(name, label));
        setType(getClass().getName());
        setSingleTypeName(getClass().getSimpleName());
        setPluralTypeName(getClass().getSimpleName() + "s");
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getLabel(), getType());
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EntityNode)) {
            return false;
        }
        if (!super.equals(object)) {
            return false;
        }
        final EntityNode that = (EntityNode) object;
        return Objects.equals(getId(), that.getId()) &&
               Objects.equals(getLabel(), that.getLabel()) &&
               Objects.equals(getType(), that.getType());
    }

    @Override
    public String toString() {
        return getType() + " " + getName();
    }
    /**
     * Contains any tags associated with this node.
     */
    @ManyToMany
    private final       Set<Tag> tags            = new HashSet<>();
    /**
     * Entity label. This is a unique label that provides a textual ID that can be used in, e.g., REST calls.
     */
    @NotNull
    @Size(min = 1, max = 64)
    @Pattern(regexp = "^[\\w-]+$", message = "The entity label must consist of 1 to 64 upper- or lower-case letters, digits, or the '_' and '-' characters.")
    @Column(unique = true, nullable = false, length = 64)
    private             String   label;
    /**
     * Contains a readable entity name. By default, this is set to the entity label if it's not explicitly set.
     */
    @NotNull
    @Size(max = 255)
    @Pattern(regexp = "^[\\p{Graph}\\p{Space}]+$", message = "The subject name may be left empty, but if not can contain up to 255 characters.")
    private             String   name;
    /**
     * Entity type. This is a unique label that provides a textual ID that can be used to group entities of the same
     * type. By default this is set to the fully qualified class name of the entity type.
     */
    @NotNull
    @Size(min = 2, max = 100)
    @Pattern(regexp = "^[\\w-.]+$", message = "The entity type must consist of 2 to 100 upper- or lower-case letters, digits, or the '.', '_', and '-' characters.")
    @Column(nullable = false, length = 100)
    private             String   type;
    /**
     * Gets the simple name for the entity type. By default this is set to the class name (not fully qualified) of the
     * entity.
     */
    @NotNull
    @Size(min = 2, max = 100)
    @Pattern(regexp = "^[\\w-.]+$", message = "The entity type must consist of 2 to 100 upper- or lower-case letters, digits, or the '.', '_', and '-' characters.")
    @Column(nullable = false, length = 100)
    private             String   singleTypeName;
    /**
     * Gets the simple name for multiple instances of the entity type. By default this is set to the class name (not
     * fully qualified) of the entity with 's' appended.
     */
    @NotNull
    @Size(min = 2, max = 100)
    @Pattern(regexp = "^[\\w-.]+$", message = "The entity type must consist of 2 to 100 upper- or lower-case letters, digits, or the '.', '_', and '-' characters.")
    @Column(nullable = false, length = 100)
    private             String   pluralTypeName;
}
