package org.nrg.xnat.workflows.flowable.config;

import com.fasterxml.classmate.TypeResolver;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.time.LocalDate;

import static java.util.Collections.singletonList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class)
public class WorkbenchConfig implements WebMvcConfigurer {
    @Autowired
    public WorkbenchConfig(final TypeResolver typeResolver) {
        this._typeResolver = typeResolver;
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("**/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/workbench");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/users").setViewName("users");
        registry.addViewController("/groups").setViewName("groups");
        registry.addViewController("/roles").setViewName("roles");
    }

    @Bean
    public MessageSource messageSource() {
        return new ReloadableResourceBundleMessageSource() {{
            setBasename("classpath:messages");
        }};
    }

    @Bean
    public ServletRegistrationBean<WebServlet> h2servletRegistration() {
        return new ServletRegistrationBean<WebServlet>(new WebServlet()) {{
            addUrlMappings("/console/*");
        }};
    }

    @Bean
    public Docket workbenchApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/api")
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(_typeResolver.resolve(DeferredResult.class, _typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                            _typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, singletonList(new ResponseMessageBuilder().code(500).message("500 message").responseModel(new ModelRef("Error")).build()))
                // .securitySchemes(singletonList(apiKey()))
                // .securityContexts(singletonList(securityContext()))
                .enableUrlTemplating(true)
                /*.globalOperationParameters(singletonList(new ParameterBuilder()
                                                                 .name("someGlobalParameter")
                                                                 .description("Description of someGlobalParameter")
                                                                 .modelRef(new ModelRef("string"))
                                                                 .parameterType("query")
                                                                 .required(true)
                                                                 .build())) */
                .tags(new Tag("Workbench", "The XNAT Workbench API"));
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                                     .docExpansion(DocExpansion.NONE)
                                     .operationsSorter(OperationsSorter.ALPHA)
                                     .tagsSorter(TagsSorter.ALPHA)
                                     .defaultModelRendering(ModelRendering.MODEL)
                                     .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                                     .build();
    }

    /*
    @Bean
    public SecurityConfiguration security() {
        return new SecurityConfiguration(
                "test-app-client-id",
                "test-app-client-secret",
                "test-app-realm",
                "test-app",
                "apiKey",
                ApiKeyVehicle.HEADER,
                "api_key",
                ",");
    }

    private ApiKey apiKey() {
        return new ApiKey("apiKey", "api_key", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                              .securityReferences(defaultAuth())
                              .forPaths(PathSelectors.regex("/anyPath.*"))
                              .build();
    }

    private List<SecurityReference> defaultAuth() {
        return singletonList(new SecurityReference("apiKey", new AuthorizationScope[]{new AuthorizationScope("global", "accessEverything")}));
    }
    */
    private final TypeResolver _typeResolver;
}
