package org.nrg.xnat.workflows.flowable.services.impl.hibernate;

import org.nrg.xnat.workflows.flowable.data.domain.*;
import org.nrg.xnat.workflows.flowable.data.security.UserPrincipal;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.TagRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ExperimentAssessorRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ExperimentRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.ProjectRepository;
import org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain.SubjectRepository;
import org.nrg.xnat.workflows.flowable.services.AbstractWorkbenchService;
import org.nrg.xnat.workflows.flowable.services.IdentityManagementService;
import org.nrg.xnat.workflows.flowable.services.PermissionsService;
import org.nrg.xnat.workflows.flowable.services.WorkbenchDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Transactional
@Service
public class JpaWorkbenchDataServiceImpl extends AbstractWorkbenchService implements WorkbenchDataService {
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public JpaWorkbenchDataServiceImpl(final IdentityManagementService idmService, final PermissionsService permissionsService, final ProjectRepository projects, final SubjectRepository subjects, final ExperimentRepository experiments, final ExperimentAssessorRepository experimentAssessors, final TagRepository tags) {
        _idmService = idmService;
        _permissionsService = permissionsService;
        _projects = projects;
        _subjects = subjects;
        _experiments = experiments;
        _experimentAssessors = experimentAssessors;
        _tags = tags;
    }

    @Override
    public List<Project> getProjects() {
        return iterableToList(_projects.findAll());
    }

    @Override
    public Long getProjectId(final String name) {
        // TODO: Implement this using projections later.
        return _projects.findByName(name).getId();
    }

    @Override
    public Project getProject(final long id) {
        return _projects.findById(id).orElseThrow(() -> new EntityNotFoundException("No project found with ID " + id));
    }

    @Override
    public Project getProject(final String label) {
        return _projects.findByLabel(label);
    }

    @Override
    public Project saveProject(final UserPrincipal user, final Project project) {
        final boolean isNewEntity = project.getId() == 0;
        _projects.save(project);
        _idmService.createStandardEntityRoles(user, project);
        if (isNewEntity) {
            _permissionsService.addOwner(user, project);
        }
        return project;
    }

    @Override
    public void deleteProject(final UserPrincipal user, final long id) {
        _projects.deleteById(id);
        _permissionsService.deleteAcl(Project.class, id);
    }

    @Override
    public void deleteProject(final UserPrincipal user, final String label) {
        deleteProject(user, _projects.findByLabel(label));
    }

    @Override
    public void deleteProject(final UserPrincipal user, final Project project) {
        deleteProject(user, project.getId());
    }

    @Override
    public List<Subject> getSubjects() {
        return iterableToList(_subjects.findAll());
    }

    @Override
    public Subject getSubject(final long id) {
        return _subjects.findById(id).orElseThrow(() -> new EntityNotFoundException("No subject found with ID " + id));
    }

    @Override
    public Subject getSubject(final String label) {
        return _subjects.findByLabel(label);
    }

    @Override
    public Subject saveSubject(final UserPrincipal user, final Subject subject) {
        if (subject.getProject() == null) {
            throw new RuntimeException("You must specify an owning project for a subject");
        }
        _subjects.save(subject);
        final MutableAcl projectAcl = _permissionsService.getEntityAcl(subject.getProject());
        final MutableAcl subjectAcl = _permissionsService.createAcl(_permissionsService.getIdentity(subject));
        subjectAcl.setParent(projectAcl);
        _permissionsService.updateAcl(subjectAcl);
        return subject;
    }

    @Override
    public void deleteSubject(final UserPrincipal user, final long id) {
        _subjects.deleteById(id);
        _permissionsService.deleteAcl(Subject.class, id);
    }

    @Override
    public void deleteSubject(final UserPrincipal user, final String label) {
        deleteSubject(user, _subjects.findByLabel(label));
    }

    @Override
    public void deleteSubject(final UserPrincipal user, final Subject subject) {
        final long id = subject.getId();
        _subjects.delete(subject);
        _permissionsService.deleteAcl(Subject.class, id);
    }

    @Override
    public List<Experiment> getExperiments() {
        return iterableToList(_experiments.findAll());
    }

    @Override
    public Experiment getExperiment(final long id) {
        return _experiments.findById(id).orElseThrow(() -> new EntityNotFoundException("No experiment found with ID " + id));
    }

    @Override
    public Experiment getExperiment(final String label) {
        return _experiments.findByLabel(label);
    }

    @Override
    public Experiment saveExperiment(final UserPrincipal user, final Experiment experiment) {
        if (experiment.getSubject() == null) {
            throw new RuntimeException("You must specify an owning subject for an experiment");
        }
        _experiments.save(experiment);
        final MutableAcl subjectAcl    = _permissionsService.getEntityAcl(experiment.getSubject());
        final MutableAcl experimentAcl = _permissionsService.createAcl(_permissionsService.getIdentity(experiment));
        experimentAcl.setParent(subjectAcl);
        _permissionsService.updateAcl(experimentAcl);
        return experiment;
    }

    @Override
    public void deleteExperiment(final UserPrincipal user, final long id) {
        _experiments.deleteById(id);
        _permissionsService.deleteAcl(Experiment.class, id);
    }

    @Override
    public void deleteExperiment(final UserPrincipal user, final String label) {
        deleteExperiment(user, _experiments.findByLabel(label));
    }

    @Override
    public void deleteExperiment(final UserPrincipal user, final Experiment experiment) {
        final long id = experiment.getId();
        _experiments.delete(experiment);
        _permissionsService.deleteAcl(Experiment.class, id);
    }

    @Override
    public List<ExperimentAssessor> getExperimentAssessors() {
        return iterableToList(_experimentAssessors.findAll());
    }

    @Override
    public ExperimentAssessor getExperimentAssessor(final long id) {
        return _experimentAssessors.findById(id).orElseThrow(() -> new EntityNotFoundException("No experiment assessor found with ID " + id));
    }

    @Override
    public ExperimentAssessor getExperimentAssessor(final String label) {
        return _experimentAssessors.findByLabel(label);
    }

    @Override
    public ExperimentAssessor saveExperimentAssessor(final UserPrincipal user, final ExperimentAssessor experimentAssessor) {
        if (experimentAssessor.getExperiment() == null) {
            throw new RuntimeException("You must specify an owning experiment for an experiment assessor.");
        }
        _experimentAssessors.save(experimentAssessor);
        final MutableAcl experimentAcl = _permissionsService.getEntityAcl(experimentAssessor.getExperiment());
        final MutableAcl assessorAcl   = _permissionsService.createAcl(_permissionsService.getIdentity(experimentAssessor));
        assessorAcl.setParent(experimentAcl);
        _permissionsService.updateAcl(assessorAcl);
        return experimentAssessor;
    }

    @Override
    public void deleteExperimentAssessor(final UserPrincipal user, final long id) {
        _experimentAssessors.deleteById(id);
        _permissionsService.deleteAcl(ExperimentAssessor.class, id);
    }

    @Override
    public void deleteExperimentAssessor(final UserPrincipal user, final String label) {
        deleteExperimentAssessor(user, _experimentAssessors.findByLabel(label));
    }

    @Override
    public void deleteExperimentAssessor(final UserPrincipal user, final ExperimentAssessor experimentAssessor) {
        final long id = experimentAssessor.getId();
        _experimentAssessors.delete(experimentAssessor);
        _permissionsService.deleteAcl(ExperimentAssessor.class, id);
    }

    @Override
    public List<Tag> getTags() {
        return iterableToList(_tags.findAll());
    }

    @Override
    public Tag getTag(final long id) {
        return _tags.findById(id).orElseThrow(() -> new EntityNotFoundException("No tag found with ID " + id));
    }

    @Override
    public Tag getTag(final String value) {
        return _tags.findByValue(value);
    }

    @Override
    public Tag saveTag(final UserPrincipal user, final String value, final String newValue) {
        final Tag tag = _tags.findByValue(value);
        tag.setValue(newValue);
        _tags.save(tag);
        return tag;
    }

    @Override
    public Tag saveTag(final UserPrincipal user, final String value) {
        final Tag tag = new Tag();
        tag.setValue(value);
        _tags.save(tag);
        return tag;
    }

    @Override
    public Tag saveTag(final UserPrincipal user, final Tag tag) {
        _tags.save(tag);
        return tag;
    }

    @Override
    public void deleteTag(final UserPrincipal user, final long id) {
        _tags.deleteById(id);
    }

    @Override
    public void deleteTag(final UserPrincipal user, final String value) {
        _tags.delete(_tags.findByValue(value));
    }

    @Override
    public void deleteTag(final UserPrincipal user, final Tag tag) {
        _tags.delete(tag);
    }

    @Override
    public <T extends EntityNode> void addOwner(final UserPrincipal actor, final UserPrincipal user, final T entity) {
        _permissionsService.addOwner(user, entity);
    }

    @Override
    public <T extends EntityNode> void addOwners(final UserPrincipal actor, final List<UserPrincipal> users, final T entity) {
        users.forEach(user -> addOwner(actor, user, entity));
    }

    @Override
    public <T extends EntityNode> void addMember(final UserPrincipal actor, final UserPrincipal user, final T entity) {
        _permissionsService.addMember(user, entity);
    }

    @Override
    public <T extends EntityNode> void addMembers(final UserPrincipal actor, final List<UserPrincipal> users, final T entity) {
        users.forEach(user -> addMember(actor, user, entity));
    }

    @Override
    public <T extends EntityNode> void addCollaborator(final UserPrincipal actor, final UserPrincipal user, final T entity) {
        _permissionsService.addCollaborator(user, entity);
    }

    @Override
    public <T extends EntityNode> void addCollaborators(final UserPrincipal actor, final List<UserPrincipal> users, final T entity) {
        users.forEach(user -> addCollaborator(actor, user, entity));
    }

    @Override
    public List<? extends EntityNode> getNodeChildren(final String type, final long parentId) {
        switch (type) {
            case "org.nrg.xnat.workflows.flowable.data.domain.Project":
                return _subjects.findAllByProject(_projects.findById(parentId).orElseThrow(() -> new EntityNotFoundException("No project found with ID " + parentId)));
            case "org.nrg.xnat.workflows.flowable.data.domain.Subject":
                return _experiments.findAllBySubject(_subjects.findById(parentId).orElseThrow(() -> new EntityNotFoundException("No subject found with ID " + parentId)));
            case "org.nrg.xnat.workflows.flowable.data.domain.Experiment":
                return _experimentAssessors.findAllByExperiment(_experiments.findById(parentId).orElseThrow(() -> new EntityNotFoundException("No experiment found with ID " + parentId)));
            case "org.nrg.xnat.workflows.flowable.data.domain.ExperimentAssessor":
                return Collections.emptyList();
            default:
                throw new RuntimeException("Unknown type: " + type);
        }
    }

    private final IdentityManagementService    _idmService;
    private final PermissionsService           _permissionsService;
    private final ProjectRepository            _projects;
    private final SubjectRepository            _subjects;
    private final ExperimentRepository         _experiments;
    private final ExperimentAssessorRepository _experimentAssessors;
    private final TagRepository                _tags;
}
