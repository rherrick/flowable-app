package org.nrg.xnat.workflows.flowable.services.impl.hibernate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.AuditLogger;
import org.springframework.security.acls.domain.DefaultPermissionGrantingStrategy;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JpaIdmPermissionGrantingStrategy extends DefaultPermissionGrantingStrategy {
    @Autowired
    public JpaIdmPermissionGrantingStrategy(final AuditLogger auditLogger) {
        super(auditLogger);
        _auditLogger = auditLogger;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isGranted(final Acl acl, final List<Permission> permissions, final List<Sid> sids, final boolean administrativeMode) throws NotFoundException {
        return super.isGranted(acl, permissions, sids, administrativeMode);
    }
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private final AuditLogger _auditLogger;
}
