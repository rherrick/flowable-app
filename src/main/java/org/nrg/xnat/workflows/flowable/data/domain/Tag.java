package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Tag extends BaseMetadata {
    /**
     * The value of this tag.
     */
    @NotNull
    private String value;

    /**
     * Any related tags.
     */
    @ElementCollection
    private Set<Tag> related = new HashSet<>();
}
