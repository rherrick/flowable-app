package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExperimentAssessor extends EntityNode {
    @SuppressWarnings("unused")
    public ExperimentAssessor(final String label, final Experiment experiment) {
        super(label);
        setExperiment(experiment);
    }

    @Override
    public String toString() {
        return "Experiment assessor " + getName();
    }
    /**
     * Specifies the parent of this node. This is null for root nodes.
     */
    @ManyToOne
    private Experiment experiment;
}
