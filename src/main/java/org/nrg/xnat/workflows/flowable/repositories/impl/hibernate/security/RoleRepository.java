package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.security.Role;
import org.nrg.xnat.workflows.flowable.repositories.WorkbenchRepository;

import java.util.List;

public interface RoleRepository extends WorkbenchRepository<Role>, CustomRoleRepository {
    boolean existsByName(final String name);

    Role findByName(final String name);

    List<Role> findAllByNameIn(final List<String> roles);

    Role findByNameAndEntityClassAndEntityId(final String name, final String entityClass, final long entityId);
}
