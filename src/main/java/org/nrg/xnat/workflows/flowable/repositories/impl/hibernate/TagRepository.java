package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate;

import org.nrg.xnat.workflows.flowable.data.domain.Tag;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused")
public interface TagRepository extends CrudRepository<Tag, Long> {
    Tag findByValue(final String value);

    Tag findByValueIn(final Collection<String> values);

    List<Tag> findByRelatedContains(final String value);
}
