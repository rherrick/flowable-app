package org.nrg.xnat.workflows.flowable.delegates;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

@Slf4j
public class DispatchDataReleaseRequestDelegate implements JavaDelegate {
    @Override
    public void execute(final DelegateExecution execution) {
        log.debug("Handling a delegate execution call: {}", execution.getId());
    }
}
