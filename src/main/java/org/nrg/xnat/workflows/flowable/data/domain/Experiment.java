package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Experiment extends EntityNode {
    public Experiment(final String label, final Subject subject) {
        this(label, null, subject);
    }
    public Experiment(final String label, final String name, final Subject subject) {
        super(label, name);
        setSubject(subject);
    }

    @Override
    public String toString() {
        return "Experiment " + getName();
    }

    // /**
    //  * Contains any children of this node.
    //  */
    // @OneToMany(mappedBy = "experiment")
    // private Set<ExperimentAssessor> experimentAssessors;
    private static final String  DESCRIPTION_MESSAGE = "The experiment notes may be left empty, but if not can contain up to " + MAX_DESCRIPTION + " characters.";
    /**
     * Specifies the subject of this node. This is null for root nodes.
     */
    @ManyToOne
    private              Subject subject;
    /**
     * Contains the date the experiment was performed.
     */
    @Past
    private              Date    dateOfAcquisition;
    /**
     * Contains the date the experiment was entered into the system.
     */
    @Past
    private Date   dateOfEntry;
    /**
     * Indicates the duration of the experiment in seconds.
     */
    private long   duration;
    /**
     * Contains notes for the experiment. This may include Markdown-formatted text.
     */
    @Size(max = EntityNode.MAX_DESCRIPTION)
    @Pattern(regexp = "^[\\p{Graph}\\p{Space}]*$", message = DESCRIPTION_MESSAGE)
    @Column(length = EntityNode.MAX_DESCRIPTION)
    private String description;
    /**
     * Contains data specific to the experiment as JSON.
     */
    //@Type(type = "com.marvinformatics.hibernate.json.JsonUserType")
    //private JsonNode data;
    private String data;
}
