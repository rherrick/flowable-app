package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.security;

import org.nrg.xnat.workflows.flowable.data.security.RoleTemplate;
import org.nrg.xnat.workflows.flowable.repositories.WorkbenchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleTemplateRepository extends WorkbenchRepository<RoleTemplate> {
    boolean existsByName(final String name);

    RoleTemplate findByName(final String name);

    List<RoleTemplate> findAllByNameIn(final List<String> roles);
}
