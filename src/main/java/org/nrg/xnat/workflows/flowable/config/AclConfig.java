package org.nrg.xnat.workflows.flowable.config;

import org.nrg.xnat.workflows.flowable.services.impl.hibernate.JpaIdmPermissionGrantingStrategy;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.acls.AclPermissionCacheOptimizer;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.domain.*;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.jdbc.LookupStrategy;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.AclService;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.sql.DataSource;
import java.util.Arrays;

// Note that the main security adapter is in AppConfig, since Spring Security needs to be configured very early, too
// early for there to be a servlet context, which is required for constructing some of the beans in here.
@Configuration
public class AclConfig {
    @Bean
    public AuditLogger auditLogger() {
        return new ConsoleAuditLogger();
    }

    @Bean
    public LookupStrategy lookupStrategy(final DataSource dataSource) {
        return new BasicLookupStrategy(dataSource, aclCache(), aclAuthorizationStrategy(), grantingStrategy());
    }

    @Bean
    public PermissionGrantingStrategy grantingStrategy() {
        return new JpaIdmPermissionGrantingStrategy(auditLogger());
    }

    @Bean
    public AclCache aclCache() {
        return new SpringCacheBasedAclCache(new ConcurrentMapCache("acl"), grantingStrategy(), aclAuthorizationStrategy());
    }

    @Bean
    public AclAuthorizationStrategy aclAuthorizationStrategy() {
        return new AclAuthorizationStrategyImpl(new SimpleGrantedAuthority("administrator"));
    }

    @Bean
    public MutableAclService aclService(final Environment environment, final DataSource dataSource, final LookupStrategy lookupStrategy, final AclCache aclCache) {
        final JdbcMutableAclService service = new JdbcMutableAclService(dataSource, lookupStrategy, aclCache);

        // If postgresql is in the environment, we need to set the identity queries for PostgreSQL because it has a
        // whole different thing going on with these identity queries.
        if (Arrays.asList(environment.getActiveProfiles()).contains("postgresql")) {
            service.setClassIdentityQuery("select currval(pg_get_serial_sequence('acl_class', 'id'))");
            service.setSidIdentityQuery("SELECT currval(pg_get_serial_sequence('acl_sid', 'id'))");
            service.setObjectIdentityPrimaryKeyQuery("SELECT acl_object_identity.id FROM acl_object_identity, acl_class WHERE acl_object_identity.object_id_class = acl_class.id AND acl_class.class=? AND acl_object_identity.object_id_identity = cast(? AS VARCHAR)");
            service.setFindChildrenQuery("SELECT obj.object_id_identity AS obj_id, class.class AS class FROM acl_object_identity obj, acl_object_identity parent, acl_class class WHERE obj.parent_object = parent.id AND obj.object_id_class = class.id AND parent.object_id_identity = cast(? AS VARCHAR) AND parent.object_id_class = (select id FROM acl_class WHERE acl_class.class = ?)");
        }

        return service;
    }

    @Bean
    public MethodSecurityExpressionHandler methodSecurityExpressionHandler(final AclService aclService) {
        final DefaultMethodSecurityExpressionHandler handler = new DefaultMethodSecurityExpressionHandler();
        handler.setDefaultRolePrefix("");
        handler.setPermissionCacheOptimizer(new AclPermissionCacheOptimizer(aclService));
        handler.setPermissionEvaluator(new AclPermissionEvaluator(aclService));
        return handler;
    }
}
