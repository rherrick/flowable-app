package org.nrg.xnat.workflows.flowable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlowableApplication {
    public static void main(final String[] args) {
        SpringApplication.run(FlowableApplication.class, args);
    }
}
