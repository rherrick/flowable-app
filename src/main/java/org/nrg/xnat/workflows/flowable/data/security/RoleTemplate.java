package org.nrg.xnat.workflows.flowable.data.security;

import lombok.*;
import org.nrg.xnat.workflows.flowable.data.AggregatedPermission;
import org.nrg.xnat.workflows.flowable.data.BaseMetadata;
import org.springframework.security.acls.model.Permission;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@SuppressWarnings("unused")
@Entity
@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class RoleTemplate extends BaseMetadata {
    public RoleTemplate(final String name, final boolean requireAssociatedObject) {
        setName(name);
        setPermissions(new AggregatedPermission(permissions));
        setRequireAssociatedObject(requireAssociatedObject);
    }
    public RoleTemplate(final String name, final boolean requireAssociatedObject, final Permission permissions) {
        setName(name);
        setPermissions(new AggregatedPermission(permissions));
        setRequireAssociatedObject(requireAssociatedObject);
    }

    /**
     * Gets the permissions for this role.
     */
    @Transient
    public AggregatedPermission getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions for this role. Note that this completely replaces any existing permissions settings.
     *
     * @param permissions The permissions to set for the role.
     */
    public void setPermissions(final Permission permissions) {
        this.permissions.clear();
        this.permissions.set(permissions);
        mask = this.permissions.getMask();
    }

    /**
     * Adds the specific permission(s) to the permissions already set for this role.
     *
     * @param permissions The permission(s) to add for this role.
     */
    public void addPermissions(final Permission permissions) {
        this.permissions.set(permissions);
        mask = permissions.getMask();
    }

    /**
     * Removes the permission specified in the submitted parameter from the permissions set for this role.
     *
     * @param permissions The permissions to remove from this role.
     */
    public void removePermissions(final Permission permissions) {
        this.permissions.clear(permissions);
        mask = permissions.getMask();
    }
    private final transient AggregatedPermission permissions = new AggregatedPermission();
    /**
     * The name of the role template and roles created from this template.
     */
    @NonNull
    @Column(nullable = false)
    private                 String               name;
    /**
     * A description of the role template.
     */
    private                 String               description;
    /**
     * Indicates whether roles based on this template should have an associated object.
     */
    @Column(nullable = false)
    private                 boolean              requireAssociatedObject;
    /**
     * The default permissions to set for roles created from this template.
     */
    @Column(nullable = false)
    private                 int                  mask;

}
