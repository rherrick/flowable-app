package org.nrg.xnat.workflows.flowable.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * This doesn't work properly, needs to be managed to keep from being discovered, e.g. https://stackoverflow.com/questions/43350397/spring-data-jpa-inheritance-in-repositories
 */
@NoRepositoryBean
public interface WorkbenchRepository<T extends Serializable> extends CrudRepository<T, Long> {
}
