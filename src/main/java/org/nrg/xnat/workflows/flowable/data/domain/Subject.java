package org.nrg.xnat.workflows.flowable.data.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Subject extends EntityNode {
    public Subject(@NotNull final String label, @NotNull final Project project) {
        this(label, null, project);
    }
    public Subject(@NotNull final String label, final String name, @NotNull final Project project) {
        super(label, name);
        setProject(project);
    }

    @Override
    public String toString() {
        return "Subject " + getName();
    }
    /**
     * Specifies the parent of this node. This is null for root nodes.
     */
    @ManyToOne
    private Project project;
    /**
     * Contains the subject's date of birth. This may be left empty.
     */
    @Past
    private Date    dateOfBirth;
    /**
     * Contains the subject's year of birth. Although this returns a date, only the year should be considered relevant.
     */
    @Past
    private Date    yearOfBirth;
    /**
     * Indicates the subject's age in years at the time of study.
     */
    @Min(0)
    @Max(150)
    private int     age;
    /**
     * Indicates age range containing the subject's age at the time of study. The meaning of the value of this property
     * is dependent on how the age ranges are defined.
     */
    @Min(0)
    private int     ageRange;
    /**
     * Indicates subject gender.
     */
    private String  gender;
    /**
     * Contains data specific to the subject as JSON.
     */
    //@Type(type = "com.marvinformatics.hibernate.json.JsonUserType")
    //private JsonNode data;
    private String  data;
}
