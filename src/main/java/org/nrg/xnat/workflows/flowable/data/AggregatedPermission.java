package org.nrg.xnat.workflows.flowable.data;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.CumulativePermission;
import org.springframework.security.acls.model.Permission;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AggregatedPermission extends CumulativePermission {
    public static final  Permission   READ               = new SingularPermission(BasePermission.READ.getMask());
    public static final  Permission   WRITE              = new SingularPermission(BasePermission.WRITE.getMask());
    public static final  Permission   CREATE             = new SingularPermission(BasePermission.CREATE.getMask());
    public static final  Permission   DELETE             = new SingularPermission(BasePermission.DELETE.getMask());
    public static final  Permission   ADMINISTRATION     = new SingularPermission(BasePermission.ADMINISTRATION.getMask());
    public static final  Permission[] BASE_PERMISSIONS   = new Permission[]{READ, WRITE, CREATE, DELETE, ADMINISTRATION};
    public static final  Permission   ADMIN_PERMISSIONS  = new AggregatedPermission(BASE_PERMISSIONS);
    public AggregatedPermission(final int mask) {
        this.mask = mask;
    }
    public AggregatedPermission(final Permission permissions) {
        set(permissions);
    }
    public AggregatedPermission(final Permission... permissions) {
        Stream.of(permissions).forEach(this::set);
    }

    public AggregatedPermission(final List<Permission> permissions) {
        permissions.forEach(this::set);
    }

    public List<Permission> disaggregate() {
        return IntStream.rangeClosed(0, 30).map(value -> Double.valueOf(Math.pow(2, value)).intValue()).filter(this::hasFlag).mapToObj(SingularPermission::new).collect(Collectors.toList());
    }

    /**
     * Indicates whether this aggregated permissions really does have more than one permission encoded in its mask. When
     * there are aggregated permissions, you can retrieve them as separate permission objects by calling the {@link
     * #disaggregate()} method, which returns a list of {@link SingularPermission} objects.
     *
     * @return Returns true if more than one permission is encoded in the object.
     */
    public boolean isAggregated() {
        return (mask & (mask - 1)) != 0;
    }

    private boolean hasFlag(final int flag) {
        return (mask & flag) != 0;
    }
    /**
     * The system bit is the highest practical bit available in signed 32-bit integers, which is the 31st bit.
     */
    private static final int          SYSTEM_BIT         = 0b1000000000000000000000000000000;
    private static final Permission   SYSTEM             = new SingularPermission(SYSTEM_BIT);
    public static final  Permission   SYSTEM_PERMISSIONS = new AggregatedPermission(BASE_PERMISSIONS).set(SYSTEM);
}
