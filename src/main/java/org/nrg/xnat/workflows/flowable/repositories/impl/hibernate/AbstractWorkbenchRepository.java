package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate;

import org.nrg.xnat.workflows.flowable.repositories.WorkbenchRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class AbstractWorkbenchRepository<T extends Serializable> extends SimpleJpaRepository<T, Long> implements WorkbenchRepository<T> {
    /**
     * Creates a new {@link AbstractWorkbenchRepository} for the given {@link JpaEntityInformation} and {@link EntityManager}.
     *
     * @param entityInformation must not be {@literal null}.
     * @param entityManager     must not be {@literal null}.
     */
    @SuppressWarnings({"SpringJavaAutowiringInspection", "SpringJavaInjectionPointsAutowiringInspection"})
    protected AbstractWorkbenchRepository(final JpaEntityInformation<T, ?> entityInformation, final EntityManager entityManager) {
        super(entityInformation, entityManager);
    }
}
