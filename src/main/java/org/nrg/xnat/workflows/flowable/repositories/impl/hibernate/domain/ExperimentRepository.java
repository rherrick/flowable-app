package org.nrg.xnat.workflows.flowable.repositories.impl.hibernate.domain;

import org.nrg.xnat.workflows.flowable.data.domain.Experiment;
import org.nrg.xnat.workflows.flowable.data.domain.Subject;
import org.nrg.xnat.workflows.flowable.data.domain.Tag;
import org.nrg.xnat.workflows.flowable.repositories.EntityNodeRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ExperimentRepository extends EntityNodeRepository<Experiment> {
    List<Experiment> findAllBySubject(final Subject subject);

    List<Experiment> findByType(final String type);

    List<Experiment> findByTags(final Tag... tags);

    Experiment findByLabel(final String label);

    Experiment findByDateOfAcquisitionAfter(final Date date);

    Experiment findByDateOfEntryAfter(final Date date);

    Experiment findByDataIsLikeAndCreatedByContainsAndCreatedAfter(final String data, final String createdBy, final Date created);
}
