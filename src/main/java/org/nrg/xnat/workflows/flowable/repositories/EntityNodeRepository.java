package org.nrg.xnat.workflows.flowable.repositories;

import org.nrg.xnat.workflows.flowable.data.domain.EntityNode;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * This doesn't work properly, needs to be managed to keep from being discovered, e.g. https://stackoverflow.com/questions/43350397/spring-data-jpa-inheritance-in-repositories
 */
@NoRepositoryBean
public interface EntityNodeRepository<T extends EntityNode> extends WorkbenchRepository<T> {
    /*
    List<T> findByParent(final long id);
    List<T> findByParent(final P subject);
    List<T> findByType(final String type);
    T findByLabel(final String label);
    List<T> findByTags(final Tag... tags);
    */
}
