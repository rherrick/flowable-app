package org.nrg.xnat.workflows.flowable.data.domain.projections;

public interface ProjectIdentifiersProjection {
    long getId();

    String getLabel();

    String getName();
}
