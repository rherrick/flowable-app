-- Adapted from the default createAclSchemaPostgres.sql from spring-security-acl-4.2.3.RELEASE.jar: added "IF NOT
-- EXISTS" to prevent failure on restarts.

-- drop table acl_entry;
-- drop table acl_object_identity;
-- drop table acl_class;
-- drop table acl_sid;

CREATE TABLE IF NOT EXISTS acl_sid
(
    id        BIGSERIAL    NOT NULL PRIMARY KEY,
    principal BOOLEAN      NOT NULL,
    sid       VARCHAR(100) NOT NULL,
    CONSTRAINT UNIQUE_uk_1 UNIQUE (sid, principal)
);

CREATE TABLE IF NOT EXISTS acl_class
(
    id    BIGSERIAL    NOT NULL PRIMARY KEY,
    class VARCHAR(100) NOT NULL,
    CONSTRAINT UNIQUE_uk_2 UNIQUE (class)
);

CREATE TABLE IF NOT EXISTS acl_object_identity
(
    id                 BIGSERIAL PRIMARY KEY,
    object_id_class    BIGINT      NOT NULL,
    object_id_identity VARCHAR(36) NOT NULL,
    parent_object      BIGINT,
    owner_sid          BIGINT,
    entries_inheriting BOOLEAN     NOT NULL,
    CONSTRAINT UNIQUE_uk_3 UNIQUE (object_id_class, object_id_identity),
    CONSTRAINT foreign_fk_1 FOREIGN KEY (parent_object) references acl_object_identity (id),
    CONSTRAINT foreign_fk_2 FOREIGN KEY (object_id_class) references acl_class (id),
    CONSTRAINT foreign_fk_3 FOREIGN KEY (owner_sid) references acl_sid (id)
);

CREATE TABLE IF NOT EXISTS acl_entry
(
    id                  BIGSERIAL PRIMARY KEY,
    acl_object_identity BIGINT  NOT NULL,
    ace_order           int     NOT NULL,
    sid                 BIGINT  NOT NULL,
    mask                integer NOT NULL,
    granting            boolean NOT NULL,
    audit_success       boolean NOT NULL,
    audit_failure       boolean NOT NULL,
    CONSTRAINT UNIQUE_uk_4 UNIQUE (acl_object_identity, ace_order),
    CONSTRAINT foreign_fk_4 FOREIGN KEY (acl_object_identity) references acl_object_identity (id),
    CONSTRAINT foreign_fk_5 FOREIGN KEY (sid) references acl_sid (id)
);

-- acl_sid:             1 bar
--                      2 zed
-- acl_class:           1 foo
--                      2 foo-field
-- acl_object_identity: 1 foo        Project 1
--                      2 foo-field  drug_use
-- 
-- 1   1   1
-- 1   1   2
-- 1   1   3
-- 1   1   4
-- 1   2   1
-- 2   1   1
