function encodeSearchParams(params) {
    var querystring = '';
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            if (querystring !== '') {
                querystring += '&';
            }
            querystring += key + '=' + params[key];
        }
    }
    return querystring;
}