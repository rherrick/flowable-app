SELECT obj.id                 AS id,
       cl.class               AS entityClass,
       obj.object_id_identity AS entityId
FROM acl_object_identity obj
         LEFT JOIN acl_class cl ON cl.id = obj.object_id_class;
