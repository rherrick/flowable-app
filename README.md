# Flowable Web App #

This is a proof-of-concept application for the following technologies:

* Flowable workflow engine
* Spring Boot
* Spring Security
* Spring Security ACLs
* Multiple authentication methods
* Lombok beans
* Mixed data persistence repos

